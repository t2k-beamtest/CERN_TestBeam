#include "../macros/ilc/common_header.h"
#include "TMultiGraph.h"

void electron_att() {

  Int_t T2KstyleIndex = 1;
  // Official T2K style as described in http://www.t2k.org/comm/pubboard/style/index_html
  TString localStyleName = "T2K";
  // -- WhichStyle --
  // 1 = presentation large fonts
  // 2 = presentation small fonts
  // 3 = publication/paper
  Int_t localWhichStyle = T2KstyleIndex-1;

  TStyle* t2kstyle = SetT2KStyle(localWhichStyle, localStyleName);
  gROOT->SetStyle(t2kstyle->GetName());

  TCanvas *c1 = new TCanvas("c1","c1",0, 0, 1200,900);
  TCanvas *c2 = new TCanvas("c2","c2",0, 0, 1200,900);
  TCanvas *c_tot_Ch  = new TCanvas("c_tot_Ch","c_tot_Ch",0, 0, 1200,900);
  c_tot_Ch->Divide(3, 3);

  TF1* fit        = NULL;
  //TF1 *f1 = new TF1("f1","[0]+exp([1]+[2]*x)", 100,400);
  TF1 *f1 = new TF1("f1","expo", 130,380);
  TF1* f2peak = new TF1("f2peak", "[0] * TMath::Gaus(x, [1], [2]) + [3] * TMath::Gaus(x, [4], [5])", 0, 1300);

  f2peak->SetParName(0, "Const1");
  f2peak->SetParName(1, "Mean1");
  f2peak->SetParName(2, "Sigma1");
  f2peak->SetParName(3, "Const2");
  f2peak->SetParName(4, "Mean2");
  f2peak->SetParName(5, "Sigma2");

  f2peak->SetParameters(400, 200, 40, 600, 380, 60);
  //f1->SetParameters(300,0.001);

  float toffset = 24+6-0.5;
  float tFirst[9] = {19.24,24+10+22./60,24+13+36./60,24+18+24./60, 48+9./60,48+12+30./60, 48+17+20./60,48+23+20./60,72+8};
  float tLast[9] = {24+1+40./60,24+13+36./60,24+18+24./60, 48+9./60,48+12+30./60, 48+17+20./60,48+23+20./60,72+8,72+16};


  //ANOTHER GAP HAPPENS IN RUNS 368 to 374!!!! Manually solved down.
  int tlocal = 0;

  /// THERE WAS A GAP BETWEEN RUNS 338 AND 339, SOME CODE TO FIX IT
  float tSpecialFirst = 24+6;
  float tSpecialLast = 24+9+22./60;
  ///
  
  float numfiles[9] = {8,3,4,4,3,3,4,6,4};
  int numevts[9] = {202,89,103,100,93,80,100,158,105};

  for(int j = 0; j<9; j++){
    tFirst[j] = tFirst[j]-toffset;
    tLast[j] = tLast[j]-toffset;
  }

  tSpecialLast-= toffset;
  tSpecialFirst-= toffset;

  TString prefix      = "/Users/cjesus/Desktop/FIGURES/TestBeam_DBSCAN/FIGURES/att/";
  TString prefix2      = "/Users/cjesus/Desktop/FIGURES/TestBeam_DBSCAN/FIGURES/dedx/";

  TString file_name   = "/output_att_cosmic.root";
  TString file_name2   = "/output_att_beam.root";
  TString file_name3  = "/output_adv.root";

  TString file_Pr1    = prefix + "Pr1" + file_name;
  TString file_Ele1   = prefix + "Ele1" + file_name;
  TString file_Pion1  = prefix + "Pion1" + file_name;

  TString file_Pr2    = prefix + "Pr2" + file_name;
  TString file_Ele2   = prefix + "Ele2" + file_name;
  TString file_Pion2  = prefix + "Pion2" + file_name;

  TString file_Pr3    = prefix + "Pr3" + file_name;
  TString file_Ele3   = prefix + "Ele3" + file_name;
  TString file_Pion3  = prefix + "Pion3" + file_name;

  TString file_Pr1_beam    = prefix + "Pr1" + file_name2;
  TString file_Ele1_beam   = prefix + "Ele1" + file_name2;
  TString file_Pion1_beam  = prefix + "Pion1" + file_name2;

  TString file_Pr2_beam    = prefix + "Pr2" + file_name2;
  TString file_Ele2_beam   = prefix + "Ele2" + file_name2;
  TString file_Pion2_beam  = prefix + "Pion2" + file_name2;

  TString file_Pr3_beam    = prefix + "Pr3" + file_name2;
  TString file_Ele3_beam   = prefix + "Ele3" + file_name2;
  TString file_Pion3_beam  = prefix + "Pion3" + file_name2;

  TString file_Pr1_Mn      = prefix2 + "Pr1" + file_name3;
  TString file_Ele1_Mn      = prefix2 + "Ele1" + file_name3;
  TString file_Pion1_Mn      = prefix2 + "Pion1" + file_name3;

  TString file_Pr2_Mn      = prefix2 + "Pr2" + file_name3;
  TString file_Ele2_Mn      = prefix2 + "Ele2" + file_name3;
  TString file_Pion2_Mn      = prefix2 + "Pion2" + file_name3;

  TString file_Pr3_Mn      = prefix2 + "Pr3" + file_name3;
  TString file_Ele3_Mn      = prefix2 + "Ele3" + file_name3;
  TString file_Pion3_Mn      = prefix2 + "Pion3" + file_name3;

  TFile* f_Pr1        = new TFile(file_Pr1.Data(), "READ");
  TFile* f_Ele1       = new TFile(file_Ele1.Data(), "READ");
  TFile* f_Pion1      = new TFile(file_Pion1.Data(), "READ");
  TFile* f_Pr2        = new TFile(file_Pr2.Data(), "READ");
  TFile* f_Ele2       = new TFile(file_Ele2.Data(), "READ");
  TFile* f_Pion2      = new TFile(file_Pion2.Data(), "READ");
  TFile* f_Pr3        = new TFile(file_Pr3.Data(), "READ");
  TFile* f_Ele3       = new TFile(file_Ele3.Data(), "READ");
  TFile* f_Pion3      = new TFile(file_Pion3.Data(), "READ");

  TFile* f_Pr1_beam        = new TFile(file_Pr1_beam.Data(), "READ");
  TFile* f_Ele1_beam       = new TFile(file_Ele1_beam.Data(), "READ");
  TFile* f_Pion1_beam      = new TFile(file_Pion1_beam.Data(), "READ");
  TFile* f_Pr2_beam        = new TFile(file_Pr2_beam.Data(), "READ");
  TFile* f_Ele2_beam       = new TFile(file_Ele2_beam.Data(), "READ");
  TFile* f_Pion2_beam      = new TFile(file_Pion2_beam.Data(), "READ");
  TFile* f_Pr3_beam        = new TFile(file_Pr3_beam.Data(), "READ");
  TFile* f_Ele3_beam       = new TFile(file_Ele3_beam.Data(), "READ");
  TFile* f_Pion3_beam      = new TFile(file_Pion3_beam.Data(), "READ");

  TFile* f_Pr1_Mn        = new TFile(file_Pr1_Mn.Data(), "READ");
  TFile* f_Ele1_Mn       = new TFile(file_Ele1_Mn.Data(), "READ");
  TFile* f_Pion1_Mn      = new TFile(file_Pion1_Mn.Data(), "READ");
  TFile* f_Pr2_Mn        = new TFile(file_Pr2_Mn.Data(), "READ");
  TFile* f_Ele2_Mn       = new TFile(file_Ele2_Mn.Data(), "READ");
  TFile* f_Pion2_Mn      = new TFile(file_Pion2_Mn.Data(), "READ");
  TFile* f_Pr3_Mn        = new TFile(file_Pr3_Mn.Data(), "READ");
  TFile* f_Ele3_Mn       = new TFile(file_Ele3_Mn.Data(), "READ");
  TFile* f_Pion3_Mn      = new TFile(file_Pion3_Mn.Data(), "READ");

  TH1F* h_Pr1_Ch_beam         = (TH1F*)f_Pr1_beam->Get("ChargeDist");
  TH1F* h_Ele1_Ch_beam        = (TH1F*)f_Ele1_beam->Get("ChargeDist");
  TH1F* h_Pion1_Ch_beam       = (TH1F*)f_Pion1_beam->Get("ChargeDist");

  TH1F* h_Pr2_Ch_beam          = (TH1F*)f_Pr2_beam->Get("ChargeDist");
  TH1F* h_Ele2_Ch_beam         = (TH1F*)f_Ele2_beam->Get("ChargeDist");
  TH1F* h_Pion2_Ch_beam        = (TH1F*)f_Pion2_beam->Get("ChargeDist");

  TH1F* h_Pr3_Ch_beam          = (TH1F*)f_Pr3_beam->Get("ChargeDist");
  TH1F* h_Ele3_Ch_beam         = (TH1F*)f_Ele3_beam->Get("ChargeDist");
  TH1F* h_Pion3_Ch_beam        = (TH1F*)f_Pion3_beam->Get("ChargeDist");

  TH1F* h_Pr1_Mn         = (TH1F*)f_Pr1_Mn->Get("cluster_norm_charge");
  TH1F* h_Ele1_Mn        = (TH1F*)f_Ele1_Mn->Get("cluster_norm_charge");
  TH1F* h_Pion1_Mn       = (TH1F*)f_Pion1_Mn->Get("cluster_norm_charge");

  TH1F* h_Pr2_Mn          = (TH1F*)f_Pr2_Mn->Get("cluster_norm_charge");
  TH1F* h_Ele2_Mn         = (TH1F*)f_Ele2_Mn->Get("cluster_norm_charge");
  TH1F* h_Pion2_Mn        = (TH1F*)f_Pion2_Mn->Get("cluster_norm_charge");

  TH1F* h_Pr3_Mn          = (TH1F*)f_Pr3_Mn->Get("cluster_norm_charge");
  TH1F* h_Ele3_Mn         = (TH1F*)f_Ele3_Mn->Get("cluster_norm_charge");
  TH1F* h_Pion3_Mn        = (TH1F*)f_Pion3_Mn->Get("cluster_norm_charge");
  
  TMultiGraph* mgr = new TMultiGraph();

  TGraphErrors *ele_att = new TGraphErrors("ele_att","ele_att");
  ele_att->SetMarkerStyle(20);

  TGraphErrors *gr_before = new TGraphErrors();
  TGraphErrors *gr_afterAtt = new TGraphErrors();
  TGraphErrors *gr_afterGain = new TGraphErrors();
  TGraphErrors *gr_afterBoth = new TGraphErrors();

  gr_before->SetMarkerStyle(21);
  gr_before->SetMarkerSize(1.5);

  gr_afterAtt->SetMarkerColor(kRed);
  gr_afterAtt->SetMarkerStyle(21);
  gr_afterAtt->SetMarkerSize(1.5);

  gr_afterGain->SetMarkerColor(kGreen+1);
  gr_afterGain->SetMarkerStyle(21);
  gr_afterGain->SetMarkerSize(1.5);

  gr_afterBoth->SetMarkerColor(kPink+1);
  gr_afterBoth->SetMarkerStyle(21);
  gr_afterBoth->SetMarkerSize(1.5);

  double Pr1_Max = h_Pr1_Ch_beam->GetMaximumBin()*5.3;
  double Pr2_Max = h_Pr2_Ch_beam->GetMaximumBin()*5.3;
  double Pr3_Max = h_Pr3_Ch_beam->GetMaximumBin()*5.3;
  double Ele1_Max = h_Ele1_Ch_beam->GetMaximumBin()*5.3;
  double Ele2_Max = h_Ele2_Ch_beam->GetMaximumBin()*5.3;
  double Ele3_Max = h_Ele3_Ch_beam->GetMaximumBin()*5.3;
  double Pion1_Max = h_Pion1_Ch_beam->GetMaximumBin()*5.3;
  double Pion2_Max = h_Pion2_Ch_beam->GetMaximumBin()*5.3;
  double Pion3_Max = h_Pion3_Ch_beam->GetMaximumBin()*5.3;

  cout << "Pr1_Max: " << Pr1_Max << endl;
  cout << "Pr2_Max: " << Pr2_Max << endl;
  cout << "Pr3_Max: " << Pr3_Max << endl;
  cout << "Ele1_Max: " << Ele1_Max << endl;
  cout << "Ele2_Max: " << Ele2_Max << endl;
  cout << "Ele3_Max: " << Ele3_Max << endl;
  cout << "Pion1_Max: " << Pion1_Max << endl;
  cout << "Pion2_Max: " << Pion2_Max << endl;
  cout << "Pion3_Max: " << Pion3_Max << endl;


  int mean = 0;

  // Pr1, Pr2, Pr3, Ele1, Ele2, Ele3, Pion1, Pion2, Pion3
  float timeBins[9] = {345.5,346.5,356.5,344.5,352.5,364.5,349.5,348.5,362.5};

  for (int a=0; a<=12; a++){

  char name_it[256];
  sprintf(name_it,"_%d", a);
  TString name_ext = name_it;

  TH1F* h_Pr1_Ch         = (TH1F*)f_Pr1->Get(("ChargeDist"+name_ext).Data());
  TH1F* h_Ele1_Ch        = (TH1F*)f_Ele1->Get(("ChargeDist"+name_ext).Data());
  TH1F* h_Pion1_Ch       = (TH1F*)f_Pion1->Get(("ChargeDist"+name_ext).Data());

  TH1F* h_Pr2_Ch          = (TH1F*)f_Pr2->Get(("ChargeDist"+name_ext).Data());
  TH1F* h_Ele2_Ch         = (TH1F*)f_Ele2->Get(("ChargeDist"+name_ext).Data());
  TH1F* h_Pion2_Ch        = (TH1F*)f_Pion2->Get(("ChargeDist"+name_ext).Data());

  TH1F* h_Pr3_Ch          = (TH1F*)f_Pr3->Get(("ChargeDist"+name_ext).Data());
  TH1F* h_Ele3_Ch         = (TH1F*)f_Ele3->Get(("ChargeDist"+name_ext).Data());
  TH1F* h_Pion3_Ch        = (TH1F*)f_Pion3->Get(("ChargeDist"+name_ext).Data());

  if (numfiles[2]>a) {
    c_tot_Ch->cd(3);
    h_Pr1_Ch->GetYaxis()->SetRangeUser(0,400);
    h_Pr1_Ch->SetMarkerStyle(22);
    h_Pr1_Ch->GetXaxis()->SetRangeUser(80,400);
    h_Pr1_Ch->Fit("f1","R");
    h_Pr1_Ch->Draw("P");
    fit = h_Pr1_Ch->GetFunction("f1");
    cout << "Par: " << -1./fit->GetParameter(1) << endl;
    cout << endl << "Chi2: " << fit->GetChisquare() << endl;
    cout << endl << "Ndof: " << fit->GetNDF() << endl;
    cout << endl << "Qual: " << fit->GetChisquare() / fit->GetNDF()  << endl;
    cout << endl << "att coeficient: " << (-1./fit->GetParameter(1))*150/timeBins[0] << endl;
    ele_att->SetPoint(ele_att->GetN(),tFirst[2]+(tLast[2]-tFirst[2])*(a+0.5)*(25*numfiles[2]/numevts[2])/numfiles[2], (-1./fit->GetParameter(1))*150/timeBins[0]);
    ele_att->SetPointError(ele_att->GetN()-1,0, (1./fit->GetParameter(1))*(1./fit->GetParameter(1))*fit->GetParError(1)*150/timeBins[0]); 
  }
  if (numfiles[3]>a) {
    c_tot_Ch->cd(4);
    h_Pr2_Ch->GetYaxis()->SetRangeUser(0,400);
    h_Pr2_Ch->SetMarkerStyle(22);
    h_Pr2_Ch->GetXaxis()->SetRangeUser(80,400);
    h_Pr2_Ch->Fit("f1","R");
    h_Pr2_Ch->Draw("P");
    fit = h_Pr2_Ch->GetFunction("f1");
    cout << "Par: " << -1./fit->GetParameter(1) << endl;
    cout << endl << "Chi2: " << fit->GetChisquare() << endl;
    cout << endl << "Ndof: " << fit->GetNDF() << endl;
    cout << endl << "Qual: " << fit->GetChisquare() / fit->GetNDF()  << endl;
    cout << endl << "att coeficient: " << (-1./fit->GetParameter(1))*150/timeBins[1] << endl;
    ele_att->SetPoint(ele_att->GetN(),tFirst[3]+(tLast[3]-tFirst[3])*(a+0.5)*(25*numfiles[3]/numevts[3])/numfiles[3], (-1./fit->GetParameter(1))*150/timeBins[0]);
    ele_att->SetPointError(ele_att->GetN()-1,0, (1./fit->GetParameter(1))*(1./fit->GetParameter(1))*fit->GetParError(1)*150/timeBins[1]); 
  }
  if (numfiles[6]>a) {
    c_tot_Ch->cd(7);
    h_Pr3_Ch->GetYaxis()->SetRangeUser(0,400);
    h_Pr3_Ch->SetMarkerStyle(22);
    h_Pr3_Ch->GetXaxis()->SetRangeUser(80,400);
    h_Pr3_Ch->Fit("f1","R");
    h_Pr3_Ch->Draw("P");
    fit = h_Pr3_Ch->GetFunction("f1");
    cout << "Par: " << -1./fit->GetParameter(1) << endl;
    cout << endl << "att coeficient: " << (-1./fit->GetParameter(1))*150/timeBins[2] << endl;
    ele_att->SetPoint(ele_att->GetN(),tFirst[6]+(tLast[6]-tFirst[6])*(a+0.5)*(25*numfiles[6]/numevts[6])/numfiles[6], (-1./fit->GetParameter(1))*150/timeBins[0]);
    ele_att->SetPointError(ele_att->GetN()-1,0, (1./fit->GetParameter(1))*(1./fit->GetParameter(1))*fit->GetParError(1)*150/timeBins[2]); 
  }

  if (numfiles[1]>a) {
    c_tot_Ch->cd(2);
    h_Ele1_Ch->GetYaxis()->SetRangeUser(0,400);
    h_Ele1_Ch->SetMarkerStyle(22);
    h_Ele1_Ch->GetXaxis()->SetRangeUser(80,400);
    h_Ele1_Ch->Fit("f1","R");
    h_Ele1_Ch->Draw("P");
    fit = h_Ele1_Ch->GetFunction("f1");
    cout << "Par: " << -1./fit->GetParameter(1) << endl;
    cout << endl << "att coeficient: " << (-1./fit->GetParameter(1))*150/timeBins[3] << endl;
    ele_att->SetPoint(ele_att->GetN(),tFirst[1]+(tLast[1]-tFirst[1])*(a+0.5)*(25*numfiles[1]/numevts[1])/numfiles[1], (-1./fit->GetParameter(1))*150/timeBins[0]);
    ele_att->SetPointError(ele_att->GetN()-1,0, (1./fit->GetParameter(1))*(1./fit->GetParameter(1))*fit->GetParError(1)*150/timeBins[3]); 
  }

  if (numfiles[5]>a) {
    c_tot_Ch->cd(6);
    h_Ele2_Ch->GetYaxis()->SetRangeUser(0,400);
    h_Ele2_Ch->SetMarkerStyle(22);
    h_Ele2_Ch->GetXaxis()->SetRangeUser(80,400);
    h_Ele2_Ch->Fit("f1","R");
    h_Ele2_Ch->Draw("P");
    fit = h_Ele2_Ch->GetFunction("f1");
    cout << "Par: " << -1./fit->GetParameter(1) << endl;
    cout << endl << "att coeficient: " << (-1./fit->GetParameter(1))*150/timeBins[4] << endl;
    ele_att->SetPoint(ele_att->GetN(),tFirst[5]+(tLast[5]-tFirst[5])*(a+0.5)*(25*numfiles[5]/numevts[5])/numfiles[5], (-1./fit->GetParameter(1))*150/timeBins[0]);
    ele_att->SetPointError(ele_att->GetN()-1,0, (1./fit->GetParameter(1))*(1./fit->GetParameter(1))*fit->GetParError(1)*150/timeBins[4]); 
  }

  if (numfiles[8]>a) {
    c_tot_Ch->cd(9);
    h_Ele3_Ch->GetYaxis()->SetRangeUser(0,400);
    h_Ele3_Ch->SetMarkerStyle(22);
    h_Ele3_Ch->GetXaxis()->SetRangeUser(80,400);
    h_Ele3_Ch->Fit("f1","R");
    h_Ele3_Ch->Draw("P");
    fit = h_Ele3_Ch->GetFunction("f1");
    cout << "Par: " << -1./fit->GetParameter(1) << endl;
    cout << endl << "att coeficient: " << (-1./fit->GetParameter(1))*150/timeBins[5] << endl;
    ele_att->SetPoint(ele_att->GetN(),tFirst[8]+(tLast[8]-tFirst[8])*(a+0.5)*(25*numfiles[8]/numevts[8])/numfiles[8], (-1./fit->GetParameter(1))*150/timeBins[0]);
    ele_att->SetPointError(ele_att->GetN()-1,0, (1./fit->GetParameter(1))*(1./fit->GetParameter(1))*fit->GetParError(1)*150/timeBins[5]); 
  }

  if (numfiles[0]+3>a) {
  //     if(a<=7){
  //   c_tot_Ch->cd(1);
  //   h_Pion1_Ch->GetYaxis()->SetRangeUser(0,400);
  //   h_Pion1_Ch->SetMarkerStyle(22);
  //   h_Pion1_Ch->GetXaxis()->SetRangeUser(80,400);
  //   h_Pion1_Ch->Fit("f1","R");
  //   h_Pion1_Ch->Draw("P"); 
  //   fit = h_Pion1_Ch->GetFunction("f1");
  //   cout << "Par: " << -1./fit->GetParameter(1) << endl;
  //   cout << endl << "att coeficient: " << (-1./fit->GetParameter(1))*150/timeBins[6] << endl;
  //   ele_att->SetPoint(ele_att->GetN(),tFirst[0]+(tLast[0]-tFirst[0])*(a+0.5)*(25*numfiles[0]/numevts[0])/numfiles[0], (-1./fit->GetParameter(1))*150/timeBins[0]); 
  //   ele_att->SetPointError(ele_att->GetN()-1,0, (1./fit->GetParameter(1))*(1./fit->GetParameter(1))*fit->GetParError(1)*150/timeBins[6]); 
  //   }
  if(a>8){
    c_tot_Ch->cd(1);
    h_Pion1_Ch->GetYaxis()->SetRangeUser(0,400);
    h_Pion1_Ch->SetMarkerStyle(22);
    h_Pion1_Ch->GetXaxis()->SetRangeUser(80,400);
    h_Pion1_Ch->Fit("f1","R");
    h_Pion1_Ch->Draw("P"); 
    fit = h_Pion1_Ch->GetFunction("f1");
    cout << "Par: " << -1./fit->GetParameter(1) << endl;
    cout << endl << "att coeficient: " << (-1./fit->GetParameter(1))*150/timeBins[6] << endl;
    ele_att->SetPoint(ele_att->GetN(),tSpecialFirst+(tSpecialLast-tSpecialFirst)*(a-7+0.5)*(25*3./87)/3, (-1./fit->GetParameter(1))*150/timeBins[0]); 
    ele_att->SetPointError(ele_att->GetN()-1,0, (1./fit->GetParameter(1))*(1./fit->GetParameter(1))*fit->GetParError(1)*150/timeBins[6]); 
    }
  }

  if (numfiles[4]>a) {
    if(a==0) tlocal = 49-toffset;
    if(a==1) tlocal = 48+4+40./60  -toffset;
    if(a==2) tlocal = 48+8 -toffset;
    if(a==3) tlocal = 48+9+30./60 -toffset;
    c_tot_Ch->cd(5);
    h_Pion2_Ch->GetYaxis()->SetRangeUser(0,400);
    h_Pion2_Ch->SetMarkerStyle(22);
    h_Pion2_Ch->GetXaxis()->SetRangeUser(80,400);
    h_Pion2_Ch->Fit("f1","R");
    h_Pion2_Ch->Draw("P");
    fit = h_Pion2_Ch->GetFunction("f1");
    cout << "Par: " << -1./fit->GetParameter(1) << endl;
    cout << endl << "att coeficient: " << (-1./fit->GetParameter(1))*150/timeBins[7] << endl;
    ele_att->SetPoint(ele_att->GetN(),tlocal, (-1./fit->GetParameter(1))*150/timeBins[0]);
    ele_att->SetPointError(ele_att->GetN()-1,0, (1./fit->GetParameter(1))*(1./fit->GetParameter(1))*fit->GetParError(1)*150/timeBins[7]); 
  }

  if (numfiles[7]>a) {
    c_tot_Ch->cd(8);
    h_Pion3_Ch->GetYaxis()->SetRangeUser(0,400);
    h_Pion3_Ch->SetMarkerStyle(22);
    h_Pion3_Ch->GetXaxis()->SetRangeUser(80,400);
    h_Pion3_Ch->Fit("f1","R");
    h_Pion3_Ch->Draw("P");
    fit = h_Pion3_Ch->GetFunction("f1");
    cout << "Par: " << -1./fit->GetParameter(1) << endl;
    cout << endl << "att coeficient: " << (-1./fit->GetParameter(1))*150/timeBins[8] << endl;
    ele_att->SetPoint(ele_att->GetN(),tFirst[7]+(tLast[7]-tFirst[7])*(a+0.5)*(25*numfiles[7]/numevts[7])/numfiles[7], (-1./fit->GetParameter(1))*150/timeBins[0]);
    ele_att->SetPointError(ele_att->GetN()-1,0, (1./fit->GetParameter(1))*(1./fit->GetParameter(1))*fit->GetParError(1)*150/timeBins[8]); 
  }

  }

  c_tot_Ch->Print((prefix+"ChargeDist.pdf").Data());


  //FOR THE DEDX FAIR COMPARISON: 

  // Pr1, Pr2, Pr3, Ele1, Ele2, Ele3, Pion1, Pion2, Pion3
  float attVector[9] = {1.162,1.044,1,1.175,1.040,1,1.231,1.040,1};
  float gainVector[9] = {1.005,0.995,0.984,1.026,0.969,0.997,1.14,1.011,1.009};
  float bothVector[9] = {1.168,1.039,0.984,1.206,1.008,0.997,1.403,1.051,1.009};

  h_Pr1_Mn->Fit("gaus", "", "", 380., 600.);
  fit = h_Pr1_Mn->GetFunction("gaus");
  mean  = fit->GetParameter(1);
  gr_before->SetPoint(gr_before->GetN(),80, mean);
  gr_afterAtt->SetPoint(gr_afterAtt->GetN(), 80,   mean*attVector[0]);
  gr_afterGain->SetPoint(gr_afterGain->GetN(),80, mean*gainVector[0]);
  gr_afterBoth->SetPoint(gr_afterBoth->GetN(),80, mean*bothVector[0]);
  h_Pr2_Mn->Fit("gaus", "", "", 420., 700.);
  fit = h_Pr2_Mn->GetFunction("gaus");
  mean  = fit->GetParameter(1);
  gr_before->SetPoint(gr_before->GetN(),30, mean);  
  gr_afterAtt->SetPoint(gr_afterAtt->GetN(),30,   mean*attVector[1]);
  gr_afterGain->SetPoint(gr_afterGain->GetN(),30, mean*gainVector[1]);
  gr_afterBoth->SetPoint(gr_afterBoth->GetN(),30, mean*bothVector[1]);

  h_Pr3_Mn->Fit("gaus", "", "", 450., 700.);
  fit = h_Pr3_Mn->GetFunction("gaus");
  mean  = fit->GetParameter(1);
  gr_before->SetPoint(gr_before->GetN(),10, mean);
  gr_afterAtt->SetPoint(gr_afterAtt->GetN(),10,   mean*attVector[2]);
  gr_afterGain->SetPoint(gr_afterGain->GetN(),10, mean*gainVector[2]);
  gr_afterBoth->SetPoint(gr_afterBoth->GetN(),10, mean*bothVector[2]);

  h_Ele1_Mn->Fit("gaus", "", "", 270., 420.);
  fit = h_Ele1_Mn->GetFunction("gaus");
  mean  = fit->GetParameter(1);
  gr_before->SetPoint(gr_before->GetN(),80, mean);
  gr_afterAtt->SetPoint(gr_afterAtt->GetN(),80,   mean*attVector[3]);
  gr_afterGain->SetPoint(gr_afterGain->GetN(),80, mean*gainVector[3]);
  gr_afterBoth->SetPoint(gr_afterBoth->GetN(),80, mean*bothVector[3]);

  h_Ele2_Mn->Fit("gaus", "", "", 300., 500.);
  fit = h_Ele2_Mn->GetFunction("gaus");
  mean  = fit->GetParameter(1);
  gr_before->SetPoint(gr_before->GetN(),30, mean);  
  gr_afterAtt->SetPoint(gr_afterAtt->GetN(),30,   mean*attVector[4]);
  gr_afterGain->SetPoint(gr_afterGain->GetN(),30, mean*gainVector[4]);
  gr_afterBoth->SetPoint(gr_afterBoth->GetN(),30, mean*bothVector[4]);

  h_Ele3_Mn->Fit("gaus", "", "", 300., 500.);
  fit = h_Ele3_Mn->GetFunction("gaus");
  mean  = fit->GetParameter(1);
  gr_before->SetPoint(gr_before->GetN(),10, mean);
  gr_afterAtt->SetPoint(gr_afterAtt->GetN(),10,   mean*attVector[5]);
  gr_afterGain->SetPoint(gr_afterGain->GetN(),10, mean*gainVector[5]);
  gr_afterBoth->SetPoint(gr_afterBoth->GetN(),10, mean*bothVector[5]);
  
  h_Pion1_Mn->Fit("f2peak", "", "", 0., 600.);
  fit = h_Pion1_Mn->GetFunction("f2peak");
  mean  = fit->GetParameter(1);
  gr_before->SetPoint(gr_before->GetN(),80, mean);
  gr_afterAtt->SetPoint(gr_afterAtt->GetN(),80,   mean*attVector[6]);
  gr_afterGain->SetPoint(gr_afterGain->GetN(),80, mean*gainVector[6]);
  gr_afterBoth->SetPoint(gr_afterBoth->GetN(),80, mean*bothVector[6]);

  h_Pion2_Mn->Fit("f2peak", "", "", 0., 600.);
  fit = h_Pion2_Mn->GetFunction("f2peak");
  mean  = fit->GetParameter(1);
  gr_before->SetPoint(gr_before->GetN(),30, mean);  
  gr_afterAtt->SetPoint(gr_afterAtt->GetN(),30,   mean*attVector[7]);
  gr_afterGain->SetPoint(gr_afterGain->GetN(),30, mean*gainVector[7]);
  gr_afterBoth->SetPoint(gr_afterBoth->GetN(),30, mean*bothVector[7]);

  h_Pion3_Mn->Fit("f2peak", "", "", 0., 600.);
  fit = h_Pion3_Mn->GetFunction("f2peak");
  mean  = fit->GetParameter(1);
  gr_before->SetPoint(gr_before->GetN(),10, mean);
  gr_afterAtt->SetPoint(gr_afterAtt->GetN(),10,   mean*attVector[8]);
  gr_afterGain->SetPoint(gr_afterGain->GetN(),10, mean*gainVector[8]);
  gr_afterBoth->SetPoint(gr_afterBoth->GetN(),10, mean*bothVector[8]);

  c1->cd();
  ele_att->GetYaxis()->SetRangeUser(0,700);
  ele_att->GetXaxis()->SetRangeUser(0,59);
  ele_att->GetYaxis()->SetTitle("Electron attenuation length [cm]");
  ele_att->GetXaxis()->SetTitle("Time [h]");
  ele_att->Draw("AP");

  TFile* fileout        = new TFile("/Users/cjesus/Desktop/out_att.root", "RECREATE");
  ele_att->Write();
  fileout->Close();

  c1->Print((prefix+"Att_Length.pdf").Data());

  c2->cd();
  mgr->Add(gr_before, "p");
  mgr->Add(gr_afterAtt, "p");
  mgr->Add(gr_afterGain, "p");
  mgr->Add(gr_afterBoth, "p");

    TLegend* l2=new TLegend(0.5+gStyle->GetPadLeftMargin(),
                          1-gStyle->GetPadTopMargin() - 0.4,
                          gStyle->GetPadLeftMargin() + 0.6,
                          1-gStyle->GetPadTopMargin()-0.7);
  
  l2->AddEntry(gr_before, "Original");
  l2->AddEntry(gr_afterGain, "Gain");
  l2->AddEntry(gr_afterAtt, "Att");
  l2->AddEntry(gr_afterBoth, "Both");

  mgr->Draw("a");
  mgr->GetYaxis()->SetRangeUser(0,700);

  l2->Draw();
  c2->Print((prefix+"dedx_att.pdf").Data());

  // TFile* out_plot_file = new TFile((prefix + "/drift_velocity.root").Data(), "RECREATE");
  // out_plot_file->Close();

}
