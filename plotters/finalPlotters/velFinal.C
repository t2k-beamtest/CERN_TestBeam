#define THIS_NAME velFinal
#define NOINTERACTIVE_OUTPUT
#define OVERRIDE_OPTIONS

#include "../../macros/ilc/common_header.h"
#include "../../macros/ilc/ini.C"
#include "TMultiGraph.h"
#include "TAxis.h"

//MEASURED VALUES FOR THE ANODE: Mean = 57.7 ; Std = 1.18; 

float anodePos = 57.5;
float anodeStd = 1.18;

double lTPC = 154.1;

TCanvas *canv  = new TCanvas("canv","canv",0, 0, 800,630);

float computeError(int val,int N){
    float a = lTPC/((val-(anodePos+anodeStd))*0.08)-lTPC/((val-(anodePos))*0.08);
    return sqrt(pow(a/sqrt(771),2)+pow(a/(sqrt(N)),2));
}

void computeVel(TGraphErrors* gr_err,TH1F* h, TString folder, Int_t Max, Int_t End, Int_t N, Int_t P, bool exception=kFALSE){

    //Data at: https://docs.google.com/spreadsheets/d/19LqDWqjK-Ssp9XulG2hYsydq9DNpOyaClsqUhrpysyM/edit#gid=0
    //Pion1,Ele1,Pr1,Pr2,Pion2,Ele2,Pr3,Pion3,Ele3,Pion4,Ele4,Pr4,Ele5,Pion5,Pr5,AntiPr,Muon,Ele6,Pion6

    float tFirst[19] = {/*0.8Gev/c 80cm*/ 24+6,24+10+22./60, 24+13+36./60,              //Pion1, Ele1, Pr1
                        /*0.8Gev/c 30cm*/ 24+18+24./60, 48+9./60,48+12+30./60,          //Pr2, Pion2, Ele2
                        /*0.8Gev/c 10cm*/ 48+17+20./60,48+23+20./60,72+8,               //Pr3, Pion3, Ele3
                        /*0.5Gev/c 30cm*/ 4*24+16+8./60, 5*24+15./60, 5*24+8+17./60,    //Pion4, Ele4, Pr4 
                        /*1.0Gev/c 30cm*/ 5*24+20+11./60, 6*24+7+40./60, 6*24+15+9./60, //Ele5, Pion5, Pr5
                       /*-0.8Gev/C 30cm*/ 7*24+12+12./60,                               //AntiPr
                        /*2.0Gev/c 30cm*/ 7*24+23+14./60, 8*24+8+33./60, 8*24+11+5./60};//Muon, Ele6, Pion6


    float tLast[19] = {/*0.8Gev/c 80cm*/ 24+9+22./60, 24+13+36./60, 24+18+24./60,       //Pion1, Ele1, Pr1
                       /*0.8Gev/c 30cm*/ 48+9./60, 48+12+30./60, 48+17+20./60,          //Pr2, Pion2, Ele2
                       /*0.8Gev/c 10cm*/ 48+23+20./60, 72+8, 72+16,                     //Pr3, Pion3, Ele3
                       /*0.5Gev/c 30cm*/ 5*24+13./60, 5*24+8+15./60, 5*24+20+3./60,     //Pion4, Ele4, Pr4 
                       /*1.0Gev/c 30cm*/ 6*24+3, 6*24+15+8./60, 6*24+21+43./60,         //Ele5, Pion5, Pr5
                      /*-0.8Gev/C 30cm*/ 7*24+13+30./60,                                //AntiPr
                       /*2.0Gev/c 30cm*/ 8*24+7+16./60, 8*24+11+2./60, 8*24+12};        //Muon, Ele6, Pion6
    
    int numevts[19] = {/*0.8Gev/c 80cm*/ 87,89,103,     //Pion1, Ele1, Pr1
                       /*0.8Gev/c 80cm*/ 100,93,80,     //Pr2, Pion2, Ele2
                       /*0.8Gev/c 80cm*/ 100,158,105,   //Pr3, Pion3, Ele3
                       /*0.5Gev/c 30cm*/ 96,103,136,    //Pion4, Ele4, Pr4
                       /*1.0Gev/c 30cm*/ 100,102,74,    //Ele5, Pion5, Pr5
                      /*-0.8Gev/C 30cm*/ 17,            //AntiPr
                       /*2.0Gev/c 30cm*/ 151,87,31};    //Muon, Ele6, Pion6 

    float numfiles[19] = {/*0.8Gev/c 80cm*/ 3,3,4,      //Pion1, Ele1, Pr1
                          /*0.8Gev/c 30cm*/ 4,3,3,      //Pr2, Pion2, Ele2
                          /*0.8Gev/c 10cm*/ 4,6,4,      //Pr3, Pion3, Ele3
                          /*0.5Gev/c 30cm*/ 3,4,5,      //Pion4, Ele4, Pr4
                          /*1.0Gev/c 30cm*/ 4,4,2,      //Ele5, Pion5, Pr5
                         /*-0.8Gev/C 30cm*/ 1,          //AntiPr
                          /*2.0Gev/c 30cm*/ 6,3,1};     //Muon, Ele6, Pion6 

    double margin = 2;
    double toffset = 24+6-margin;

    for(int j = 0; j<19; j++){              // experimental conditions info is stored in packs of 25k events (using /macros/iron.C). E.g: If one run has 80k events, the last 5k events are not used.
                                            // therefore, using the initial and final time (tFirst, tLast) of the data collection and how many groups of 25k events (numfiles)
                                            // together with the total number of events on that dataset (numevents) the datapoints are centered at the correct position. Use couts to debbug.     
        if(j==15) {
            //cout << "Skipping AntiPr" << endl << endl;
            continue; // exception for AntiPr (only have 17k evts)
        }
        //cout << "Dataset: " << j << endl;
        //cout << "ORIGINAL tLast: " << tLast[j] << endl;
        //cout << "percentage of unstudied time in the run: " << 100*(numevts[j]-25*numfiles[j])/numevts[j] << endl;
        tLast[j] = tLast[j]-(tLast[j]-tFirst[j])*(numevts[j]-25*numfiles[j])/numevts[j];
        //cout << "CORRECTED tLast: " << tLast[j] << endl << endl;
    }

    for(int j = 0; j<19; j++){        //correct the time offset for all numbers.
      tFirst[j] = tFirst[j]-toffset;
      tLast[j] = tLast[j]-toffset;
    }

    canv->cd();
    h->Draw("HIST");
    canv->SaveAs(TString::Format("%s/Vel_%d.pdf",folder.Data(),P).Data());

    if(Max <= 1) return;
    //if(End > 490) return;

    cout << folder.Data() << ":" << endl;
    cout << "timeBins: " << End << endl;
    cout << "Drift velocity [a.u]: " << 1/(End-anodePos) << endl;
    cout << "Drift velocity [cm/us]: " << lTPC/((End-anodePos)*0.08) << endl;
    
    if(!exception){
                gr_err->SetPoint(gr_err->GetN(), tFirst[N]+(tLast[N]-tFirst[N])*(P+0.5)*(25*numfiles[N]/numevts[N])/numfiles[N], lTPC/((End-anodePos)*0.08));
                gr_err->SetPointError(gr_err->GetN()-1,0, computeError(End,Max)); 
    }
    else{
          //GAP IN RUNS 368 to 374 --> Solved using this exception.
          double tlocal = 0;
          if(P==0) tlocal = 49-toffset;
          else if(P==1) tlocal = 48+4+40./60  -toffset;
          else if(P==2) tlocal = 48+8 -toffset;
          else if(P==3) tlocal = 48+9+30./60 -toffset;
          else { cout << "Problem in the exception run 'Pion2'!: iteration is too large." << endl << endl; exit(1); }
          gr_err->SetPoint(gr_err->GetN(), tlocal, lTPC/((End-anodePos)*0.08));
          gr_err->SetPointError(gr_err->GetN()-1,0, computeError(End,Max)); 
    }

}

void velFinal() {

    Int_t T2KstyleIndex = 3;
    // Official T2K style as described in http://www.t2k.org/comm/pubboard/style/index_html
    TString localStyleName = "T2K";
    // -- WhichStyle --
    // 1 = presentation large fonts
    // 2 = presentation small fonts
    // 3 = publication/paper
    Int_t localWhichStyle = T2KstyleIndex;

    TStyle* t2kstyle = SetT2KStyle(localWhichStyle, localStyleName);
    gROOT->SetStyle(t2kstyle->GetName());
    gROOT->ForceStyle(t2kstyle);

    TCanvas *c_gr  = new TCanvas("c_gr","c_gr",0, 0, 800,630);

    float numfiles[19] = {/*0.8Gev/c 80cm*/ 3,3,4,     //Pion1, Ele1, Pr1
                          /*0.8Gev/c 30cm*/ 4,3,3,     //Pr2, Pion2, Ele2
                          /*0.8Gev/c 10cm*/ 4,6,4,     //Pr3, Pion3, Ele3
                          /*0.5Gev/c 30cm*/ 3,4,5,     //Pion4, Ele4, Pr4
                          /*1.0Gev/c 30cm*/ 4,4,2,     //Ele5, Pion5, Pr5
                         /*-0.8Gev/C 30cm*/ 1,         //AntiPr
                          /*2.0Gev/c 30cm*/ 6,3,1};    //Muon, Ele6, Pion6 

    TGraphErrors *gr_a = new TGraphErrors();  //0.5 GeV/c
    TGraphErrors *gr_b = new TGraphErrors();  //0.8 GeV/c
    TGraphErrors *gr_c = new TGraphErrors();  //1.0 GeV/c
    TGraphErrors *gr_d = new TGraphErrors();  //2.0 GeV/c
    TGraphErrors *gr_e = new TGraphErrors();  //-0.8 GeV/c

    gr_a->SetMarkerColor(kBlack);
    gr_b->SetMarkerColor(kBlue);
    gr_c->SetMarkerColor(kRed);
    gr_d->SetMarkerColor(kGreen);
    gr_e->SetMarkerColor(kCyan);

    gr_a->SetMarkerStyle(20);
    gr_b->SetMarkerStyle(20);
    gr_c->SetMarkerStyle(20);
    gr_d->SetMarkerStyle(20);
    gr_e->SetMarkerStyle(20);

    gr_a->SetMarkerSize(1);
    gr_b->SetMarkerSize(1);
    gr_c->SetMarkerSize(1);
    gr_d->SetMarkerSize(1);
    gr_e->SetMarkerSize(1);

    TMultiGraph* mgr_vel = new TMultiGraph();

    TString prefix      = "/Users/cjesus/Desktop/TPC_beamtest/FIGURES/vel/";
    TString file_name   = "/output_vel_cosmic.root";

    TString file_Pr1    = prefix + "Pr1" + file_name;
    TString file_Ele1   = prefix + "Ele1" + file_name;
    TString file_Pion1  = prefix + "Pion1" + file_name;
    TString file_Pr2    = prefix + "Pr2" + file_name;
    TString file_Ele2   = prefix + "Ele2" + file_name;
    TString file_Pion2  = prefix + "Pion2" + file_name;

    TString file_Pr3    = prefix + "Pr3" + file_name;
    TString file_Ele3   = prefix + "Ele3" + file_name;
    TString file_Pion3  = prefix + "Pion3" + file_name;

    TString file_Pr4    = prefix + "Pr4" + file_name;
    TString file_Ele4   = prefix + "Ele4" + file_name;
    TString file_Pion4  = prefix + "Pion4" + file_name;

    TString file_Pr5    = prefix + "Pr5" + file_name;
    TString file_Ele5   = prefix + "Ele5" + file_name;
    TString file_Pion5  = prefix + "Pion5" + file_name;

    TString file_Ele6   = prefix + "Ele6" + file_name;
    TString file_Pion6  = prefix + "Pion6" + file_name;

    TString file_Muon   = prefix + "Muon" + file_name;
    TString file_AntiPr = prefix + "AntiPr" + file_name;

    TFile* f_Pr1        = new TFile(file_Pr1.Data(), "READ");
    TFile* f_Ele1       = new TFile(file_Ele1.Data(), "READ");
    TFile* f_Pion1      = new TFile(file_Pion1.Data(), "READ");
    TFile* f_Pr2        = new TFile(file_Pr2.Data(), "READ");
    TFile* f_Ele2       = new TFile(file_Ele2.Data(), "READ");
    TFile* f_Pion2      = new TFile(file_Pion2.Data(), "READ");
    TFile* f_Pr3        = new TFile(file_Pr3.Data(), "READ");
    TFile* f_Ele3       = new TFile(file_Ele3.Data(), "READ");
    TFile* f_Pion3      = new TFile(file_Pion3.Data(), "READ");
    TFile* f_Pr4        = new TFile(file_Pr4.Data(), "READ");
    TFile* f_Ele4       = new TFile(file_Ele4.Data(), "READ");
    TFile* f_Pion4      = new TFile(file_Pion4.Data(), "READ");
    TFile* f_Pr5        = new TFile(file_Pr5.Data(), "READ");
    TFile* f_Ele5       = new TFile(file_Ele5.Data(), "READ");
    TFile* f_Pion5      = new TFile(file_Pion5.Data(), "READ");
    TFile* f_Ele6       = new TFile(file_Ele6.Data(), "READ");
    TFile* f_Pion6      = new TFile(file_Pion6.Data(), "READ");
    TFile* f_Muon       = new TFile(file_Muon.Data(), "READ");
    TFile* f_AntiPr     = new TFile(file_AntiPr.Data(), "READ");

    for (int a=0; a<=5; a++){

        char name_it[256];
        sprintf(name_it,"_%d", a);
        TString name_ext = name_it; 

        cout << endl << "----------------------------------------------" << endl;
        cout << "analyzing " << ("TimeDist"+name_ext).Data() << endl;
        cout << "----------------------------------------------" << endl << endl;

        TH1F* h_Pr1          = (TH1F*)f_Pr1->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Ele1         = (TH1F*)f_Ele1->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Pion1        = (TH1F*)f_Pion1->Get(("TimeDist"+name_ext).Data());

        TH1F* h_Pr2          = (TH1F*)f_Pr2->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Ele2         = (TH1F*)f_Ele2->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Pion2        = (TH1F*)f_Pion2->Get(("TimeDist"+name_ext).Data());

        TH1F* h_Pr3          = (TH1F*)f_Pr3->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Ele3         = (TH1F*)f_Ele3->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Pion3        = (TH1F*)f_Pion3->Get(("TimeDist"+name_ext).Data());

        TH1F* h_Pr4          = (TH1F*)f_Pr4->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Ele4         = (TH1F*)f_Ele4->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Pion4        = (TH1F*)f_Pion4->Get(("TimeDist"+name_ext).Data());

        TH1F* h_Pr5          = (TH1F*)f_Pr5->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Ele5         = (TH1F*)f_Ele5->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Pion5        = (TH1F*)f_Pion5->Get(("TimeDist"+name_ext).Data());

        TH1F* h_Ele6         = (TH1F*)f_Ele6->Get(("TimeDist"+name_ext).Data());
        TH1F* h_Pion6        = (TH1F*)f_Pion6->Get(("TimeDist"+name_ext).Data());

        TH1F* h_Muon         = (TH1F*)f_Muon->Get(("TimeDist"+name_ext).Data());
        TH1F* h_AntiPr       = (TH1F*)f_AntiPr->Get("TotalTimeDist");

        int Max_Pr1   = 0;  int Max_Pr2    = 0; int Max_Pr3   = 0; int Max_Pr4   = 0; int Max_Pr5   = 0;
        int Max_Ele1  = 0;  int Max_Ele2   = 0; int Max_Ele3  = 0; int Max_Ele4  = 0; int Max_Ele5  = 0; int Max_Ele6  = 0;
        int Max_Pion1 = 0;  int Max_Pion2  = 0; int Max_Pion3 = 0; int Max_Pion4 = 0; int Max_Pion5 = 0; int Max_Pion6 = 0;
        int Max_Muon  = 0;  int Max_AntiPr = 0;

        int End_Pr1   = 0;  int End_Pr2    = 0; int End_Pr3   = 0; int End_Pr4   = 0; int End_Pr5   = 0;
        int End_Ele1  = 0;  int End_Ele2   = 0; int End_Ele3  = 0; int End_Ele4  = 0; int End_Ele5  = 0; int End_Ele6  = 0;
        int End_Pion1 = 0;  int End_Pion2  = 0; int End_Pion3 = 0; int End_Pion4 = 0; int End_Pion5 = 0; int End_Pion6 = 0;
        int End_Muon  = 0;  int End_AntiPr = 0;

        for(int j=350; j<500; j++){ 
            if (numfiles[2]>a)   {  if(h_Pr1->GetBinContent(j+1)>Max_Pr1)        {  Max_Pr1     = h_Pr1->GetBinContent(j+1);    End_Pr1    = j+1;  }}
            if (numfiles[3]>a)   {  if(h_Pr2->GetBinContent(j+1)>Max_Pr2)        {  Max_Pr2     = h_Pr2->GetBinContent(j+1);    End_Pr2    = j+1;  }}
            if (numfiles[6]>a)   {  if(h_Pr3->GetBinContent(j+1)>Max_Pr3)        {  Max_Pr3     = h_Pr3->GetBinContent(j+1);    End_Pr3    = j+1;  }}
            if (numfiles[1]>a)   {  if(h_Ele1->GetBinContent(j+1)>Max_Ele1)      {  Max_Ele1    = h_Ele1->GetBinContent(j+1);   End_Ele1   = j+1;  }}
            if (numfiles[5]>a)   {  if(h_Ele2->GetBinContent(j+1)>Max_Ele2)      {  Max_Ele2    = h_Ele2->GetBinContent(j+1);   End_Ele2   = j+1;  }}
            if (numfiles[8]>a)   {  if(h_Ele3->GetBinContent(j+1)>Max_Ele3)      {  Max_Ele3    = h_Ele3->GetBinContent(j+1);   End_Ele3   = j+1;  }}
            if (numfiles[0]>a)   {  if(h_Pion1->GetBinContent(j+1)>Max_Pion1)    {  Max_Pion1   = h_Pion1->GetBinContent(j+1);  End_Pion1  = j+1;  }}
            if (numfiles[4]>a)   {  if(h_Pion2->GetBinContent(j+1)>Max_Pion2)    {  Max_Pion2   = h_Pion2->GetBinContent(j+1);  End_Pion2  = j+1;  }}
            if (numfiles[7]>a)   {  if(h_Pion3->GetBinContent(j+1)>Max_Pion3)    {  Max_Pion3   = h_Pion3->GetBinContent(j+1);  End_Pion3  = j+1;  }}
            if (numfiles[9]>a)   {  if(h_Pion4->GetBinContent(j+1)>Max_Pion4)    {  Max_Pion4   = h_Pion4->GetBinContent(j+1);  End_Pion4  = j+1;  }}
            if (numfiles[10]>a)  {  if(h_Ele4->GetBinContent(j+1)>Max_Ele4)      {  Max_Ele4    = h_Ele4->GetBinContent(j+1);   End_Ele4   = j+1;  }}
            if (numfiles[11]>a)  {  if(h_Pr4->GetBinContent(j+1)>Max_Pr4)        {  Max_Pr4     = h_Pr4->GetBinContent(j+1);    End_Pr4    = j+1;  }}
            if (numfiles[12]>a)  {  if(h_Ele5->GetBinContent(j+1)>Max_Ele5)      {  Max_Ele5    = h_Ele5->GetBinContent(j+1);   End_Ele5   = j+1;  }}
            if (numfiles[13]>a)  {  if(h_Pion5->GetBinContent(j+1)>Max_Pion5)    {  Max_Pion5   = h_Pion5->GetBinContent(j+1);  End_Pion5  = j+1;  }}
            if (numfiles[14]>a)  {  if(h_Pr5->GetBinContent(j+1)>Max_Pr5)        {  Max_Pr5     = h_Pr5->GetBinContent(j+1);    End_Pr5    = j+1;  }}
            if (numfiles[15]>a)  {  if(h_AntiPr->GetBinContent(j+1)>Max_AntiPr)  {  Max_AntiPr  = h_AntiPr->GetBinContent(j+1); End_AntiPr = j+1;  }}
            if (numfiles[16]>a)  {  if(h_Muon->GetBinContent(j+1)>Max_Muon)      {  Max_Muon    = h_Muon->GetBinContent(j+1);   End_Muon   = j+1;  }}
            if (numfiles[17]>a)  {  if(h_Ele6->GetBinContent(j+1)>Max_Ele6)      {  Max_Ele6    = h_Ele6->GetBinContent(j+1);   End_Ele6   = j+1;  }}
            if (numfiles[18]>a)  {  if(h_Pion6->GetBinContent(j+1)>Max_Pion6)    {  Max_Pion6   = h_Pion6->GetBinContent(j+1);  End_Pion6  = j+1;  }}
        }


        ///***** 0.8 GeV/c DATA ******///

        if (numfiles[2]>a) {   //0.8 GeV/c protons at 80cm
            TString auxName = prefix + "Pr1";
            computeVel(gr_b,h_Pr1,auxName,Max_Pr1,End_Pr1,2,a);
        }

        if (numfiles[3]>a) {   //0.8 GeV/c protons at 30cm
            TString auxName = prefix + "Pr2";
            computeVel(gr_b,h_Pr2,auxName,Max_Pr2,End_Pr2,3,a);
        }

        if (numfiles[6]>a) {   //0.8 GeV/c protons at 10cm
            TString auxName = prefix + "Pr3";
            computeVel(gr_b,h_Pr3,auxName,Max_Pr3,End_Pr3,6,a);
        }

        if (numfiles[1]>a) {   //0.8 GeV/c electrons at 80cm
            TString auxName = prefix + "Ele1";
            computeVel(gr_b,h_Ele1,auxName,Max_Ele1,End_Ele1,1,a);
        }

        if (numfiles[5]>a) {   //0.8 GeV/c electrons at 30cm
            TString auxName = prefix + "Ele2";
            computeVel(gr_b,h_Ele2,auxName,Max_Ele2,End_Ele2,5,a);
        }

        if (numfiles[8]>a) {   //0.8 GeV/c electrons at 10cm
            TString auxName = prefix + "Ele3";
            computeVel(gr_b,h_Ele3,auxName,Max_Ele3,End_Ele3,8,a);
        }

        if (numfiles[0]>a) {   //0.8 GeV/c pions at 80cm
            TString auxName = prefix + "Pion1";
            computeVel(gr_b,h_Pion1,auxName,Max_Pion1,End_Pion1,0,a);
        }

        if (numfiles[4]>a) {   //0.8 GeV/c pions at 30cm
            TString auxName = prefix + "Pion2";
            computeVel(gr_b,h_Pion2,auxName,Max_Pion2,End_Pion2,4,a,kTRUE);
        }        

        if (numfiles[7]>a) {   //0.8 GeV/c pions at 10cm
            TString auxName = prefix + "Pion3";
            computeVel(gr_b,h_Pion3,auxName,Max_Pion3,End_Pion3,7,a);
        }

        ///***** 0.5 GeV/c DATA ******///

        if (numfiles[9]>a) {   //0.8 GeV/c pions at 30cm
            TString auxName = prefix + "Pion4";
            computeVel(gr_a,h_Pion4,auxName,Max_Pion4,End_Pion4,9,a);
        }

        if (numfiles[10]>a) {   //0.8 GeV/c electrons at 30cm
            TString auxName = prefix + "Ele4";
            computeVel(gr_a,h_Ele4,auxName,Max_Ele4,End_Ele4,10,a);
        }

        if (numfiles[11]>a) {   //0.8 GeV/c protons at 30cm
            TString auxName = prefix + "Pr4";
            computeVel(gr_a,h_Pr4,auxName,Max_Pr4,End_Pr4,11,a);
        }

        ///***** 1.0 GeV/c DATA ******///

        if (numfiles[12]>a) {   //1.0 GeV/c electrons at 30cm
            TString auxName = prefix + "Ele5";
            computeVel(gr_c,h_Ele5,auxName,Max_Ele5,End_Ele5,12,a);
        }

        if (numfiles[13]>a) {   //1.0 GeV/c pions at 30cm
            TString auxName = prefix + "Pion5";
            computeVel(gr_c,h_Pion5,auxName,Max_Pion5,End_Pion5,13,a);
        }

        // if (numfiles[14]>a) {   //1.0 GeV/c pions at 30cm
        //     TString auxName = prefix + "Pr5";
        //     computeVel(gr_c,h_Pr5,auxName,Max_Pr5,End_Pr5,14,a);
        // }

        ///***** 2.0 GeV/c DATA ******///

        if (numfiles[16]>a) {   //2.0 GeV/c muons at 30cm
            TString auxName = prefix + "Muon";
            computeVel(gr_d,h_Muon,auxName,Max_Muon,End_Muon,16,a);
        }

        if (numfiles[17]>a) {   //2.0 GeV/c pions at 30cm
            TString auxName = prefix + "Ele6";
            computeVel(gr_d,h_Ele6,auxName,Max_Ele6,End_Ele6,17,a);
        }

        if (numfiles[18]>a) {   //2.0 GeV/c protons at 30cm
            TString auxName = prefix + "Pion6";
            computeVel(gr_d,h_Pion6,auxName,Max_Pion6,End_Pion6,18,a);
        }

        ///***** -0.8 GeV/c DATA ******///
        
        if (numfiles[15]>a) {   //2.0 GeV/c muons at 30cm
            TString auxName = prefix + "AntiPr";
            computeVel(gr_e,h_AntiPr,auxName,Max_AntiPr,End_AntiPr,15,a);
        }

        ///***************************///

    }

    mgr_vel->Add(gr_a, "p");
    mgr_vel->Add(gr_b, "p");
    mgr_vel->Add(gr_c, "p");
    // mgr_vel->Add(gr_d, "p");
    // mgr_vel->Add(gr_e, "p");

    c_gr->cd();

    mgr_vel->Draw("a");
    // gPad->SetTopMargin(0.02);
    // gPad->SetLeftMargin(0.08);
    // gPad->SetRightMargin(0.02);
    mgr_vel->GetYaxis()->SetTitleOffset(1.1);
    mgr_vel->GetYaxis()->SetRangeUser(4,6);
    mgr_vel->GetXaxis()->SetLimits(0,135);
    mgr_vel->GetYaxis()->SetTitle("Drift velocity [cm/#mus]");
    mgr_vel->GetXaxis()->SetTitle("Time [h]");
    TF1* fline = new TF1("fline", "4.45", -10,150);
    fline->SetLineWidth(2);
    fline->SetLineStyle(2);
    fline->SetLineColor(kBlue);

    TLegend* l=new TLegend(0.62,0.55,0.85,0.8);
    l->AddEntry(gr_a, "0.5 GeV/c");
    l->AddEntry(gr_b, "0.8 GeV/c");
    l->AddEntry(gr_c, "1.0 GeV/c");
    // l->AddEntry(gr_d, "2.0 GeV/c");
    // l->AddEntry(gr_e, "-0.8 GeV/c");
    l->SetFillStyle(0);
    l->Draw("same");
    fline->Draw("same");

    c_gr->SaveAs((prefix+"vel.C").Data());
    c_gr->Print((prefix+"vel.pdf").Data());
}
