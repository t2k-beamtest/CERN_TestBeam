#define THIS_NAME prf_analysis
#define NOINTERACTIVE_OUTPUT
#define OVERRIDE_OPTIONS

#include "../ilc/common_header.h"
#include "../selection.C"
#include "../selection/selection3D.C"
#include "../selection/selection_ana.C"

#include <iostream>
#include <fstream>
#include <unistd.h>

//#define COSMIC

//#define SEVERE_SUPPRESSION
bool SUPPRESS_ERRORS = true;

void process_mem_usage(double& vm_usage, double& resident_set)
{
    vm_usage     = 0.0;
    resident_set = 0.0;

    // the two fields we want
    unsigned long vsize;
    long rss;
    {
        std::string ignore;
        std::ifstream ifs("/proc/self/stat", std::ios_base::in);
        ifs >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
                >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
                >> ignore >> ignore >> vsize >> rss;
    }

    long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
    vm_usage = vsize / 1024.0;
    resident_set = rss * page_size_kb;
}

void prf_analysis() {

#ifdef SEVERE_SUPPRESSION
  gErrorIgnoreLevel = kSysError;
#endif

  double scan_delta   = 0.1;
  int scan_Nsteps     = 1000;
  double scan_step    = 2. * scan_delta / scan_Nsteps;

  //bool PLOT = true;
  //bool DEBUG = true;

  TF1* f1 = new TF1("f1", " [0] * exp(-4*(1-[1])*TMath::Power(x/[2], 2.)) / (1+4 * [1] * TMath::Power(x/[2], 2.) )", -3, 3);
  f1->SetParName(0, "Const");
  f1->SetParName(1, "r");
  f1->SetParName(2, "w");

  double c = 0.8;
  double r = 0.5;
  double s = 0.7;
  f1->SetParameters(c, r, s);

  TF1* f2 = new TF1("f2", "[0]*(1+[1]*x*x + [2] * x*x*x*x) / (1+[3]*x*x+[4]*x*x*x*x)", -3, 3);
  f2->SetParName(0, "Const");
  f2->SetParName(1, "a2");
  f2->SetParName(2, "a4");
  f2->SetParName(3, "b2");
  f2->SetParName(4, "b4");

  double co = 0.83;
  double a2 = -0.84;
  double a4 = 0.83;
  double b2 = 25.2;
  double b4 = 0.83;
  f2->SetParameters(co, a2, a4, b2, b4);

  TF1* function = f2;
  TString function_str = "f2";

  TF1* f2_2pad = new TF1("f2_2pad", "[0]*(1+[1]*x*x + [2] * x*x*x*x) / (1+[3]*x*x+[4]*x*x*x*x)", -3, 3);
  TF1* f2_3pad = new TF1("f2_3pad", "[0]*(1+[1]*x*x + [2] * x*x*x*x) / (1+[3]*x*x+[4]*x*x*x*x)", -3, 3);

  f2_2pad->SetParameters(co, a2, a4, b2, b4);
  f2_3pad->SetParameters(co, a2, a4, b2, b4);

  vector<TString> listOfFiles;
  TString file    = "default.root";
  TString prefix  = "~/";
  for (int iarg=0; iarg<gApplication->Argc(); iarg++){
    if (string( gApplication->Argv(iarg))=="-f" || string( gApplication->Argv(iarg))=="--file" ){
      iarg++;
      file = gApplication->Argv(iarg);
      if (file.Contains(".root")) {

        cout << "adding filename" <<" " << file << endl;
        listOfFiles.push_back(file);
      } else {
        fstream fList(file);
        if (fList.good()) {
          while (fList.good()) {
            string filename;
            getline(fList, filename);
            if (fList.eof()) break;
            listOfFiles.push_back(filename);
          }
        }
      }
    } else if (string( gApplication->Argv(iarg))=="-o" || string( gApplication->Argv(iarg))=="--output" ){
      iarg++;
      prefix = gApplication->Argv(iarg);
    } else if (string( gApplication->Argv(iarg))=="-h" || string( gApplication->Argv(iarg))=="--help" ){
      cout << "**************************************" << endl;
      cout << "Macros run options:" << endl;
      cout << "   -f || --file      input file (*.root) or filelist" << endl;
      cout << "   -o || --output    output folder for plots and out file" << endl;
      cout << "   -h || --help      print help info" << endl;
      cout << "**************************************" << endl;
    }
  }

  TFile* out_file = new TFile((prefix + "/output_prf_uni.root").Data(), "RECREATE");
  TDirectory* dir = out_file->mkdir("col_by_col");
  out_file->cd();

  TCanvas *c1 = new TCanvas("c1","evadc1",0, 0, 400,300);
  TCanvas *c2 = new TCanvas("c2","evadc2",400, 0, 400,300);
  TCanvas *c3 = new TCanvas("c3","evadc3",800, 0, 400,300);
  TCanvas *c4 = new TCanvas("c4","evadc4", 0, 400, 400,300);
  TCanvas *c5 = new TCanvas("c5","evadc5", 400, 400, 400,300);


  float prf_min   = -1.5;
  float prf_max   = 1.5;
  int prf_bin     = 120;

  float resol_min   = -0.3;
  float resol_max   = 0.3;
  int resol_bin     = 75.;

  const int Niter = 5;
  TH2F* PRF_H[Niter];
  TH2F* PRF_H_2pad[Niter];
  TH2F* PRF_H_3pad[Niter];
  TH2F* PRF_H_4pad[Niter];
  TGraphErrors* PRF_gr[Niter];
  TGraphErrors* PRF_gr_2pad[Niter];
  TGraphErrors* PRF_gr_3pad[Niter];
  TGraph* PRF_sc[Niter];
  TH1F* Chi2_track[Niter];

  double uncertainty = -1.;

  TH1F* resol_col_hist[Niter][36];
  TH1F* resol_col_hist_1[Niter][36];

  TH1F* resol_col_hist_2pad[Niter][36];
  TH1F* resol_col_hist_1_2pad[Niter][36];

  TH1F* resol_col_hist_3pad[Niter][36];
  TH1F* resol_col_hist_1_3pad[Niter][36];

  TH1F* resol_col[Niter];
  TH1F* resol_col1[Niter];
  TH1F* resol_colf[Niter];
  TH1F* resol_colf_RMS[Niter];
  TH1F* resol_colf_FWHM[Niter];

  TH1F* resol_colf_2pad[Niter];
  TH1F* resol_colf_3pad[Niter];

  TH1F* mean_col[Niter];
  TH1F* mean_col1[Niter];
  TH1F* mean_colf[Niter];

   for (Int_t i = 0; i < Niter; ++i) {
    PRF_H[i] = new TH2F(Form("PRF_%i", i),"", prf_bin, prf_min, prf_max, 102,0.,1.02);
    PRF_H_2pad[i] = new TH2F(Form("PRF_2pad_%i", i),"", prf_bin, prf_min, prf_max, 102,0.,1.02);
    PRF_H_3pad[i] = new TH2F(Form("PRF_3pad_%i", i),"", prf_bin, prf_min, prf_max, 102,0.,1.02);
    PRF_H_4pad[i] = new TH2F(Form("PRF_4pad_%i", i),"", prf_bin, prf_min, prf_max, 102,0.,1.02);

    PRF_sc[i] = new TGraph();
    PRF_sc[i]->SetName(Form("prf_graph_%i", i));
    PRF_sc[i]->SetMaximum(1.02);

    PRF_gr[i] = new TGraphErrors();
    PRF_gr[i]->SetName(Form("prf_graph_err_%i", i));
    PRF_gr[i]->SetMaximum(1.02);

    PRF_gr_2pad[i] = new TGraphErrors();
    PRF_gr_2pad[i]->SetName(Form("prf_graph_err_2pad%i", i));
    PRF_gr_2pad[i]->SetMaximum(1.02);

    PRF_gr_3pad[i] = new TGraphErrors();
    PRF_gr_3pad[i]->SetName(Form("prf_graph_err_3pad%i", i));
    PRF_gr_3pad[i]->SetMaximum(1.02);

    Chi2_track[i] = new TH1F(Form("Chi2_track_%i", i), "", 100, 0., 3.);

    for (Int_t j = 0; j < 36; ++j) {
      resol_col_hist[i][j]  = new TH1F(Form("resol_histo_%i_%i", i, j), "", resol_bin, resol_min, resol_max);
      resol_col_hist_1[i][j]  = new TH1F(Form("resol_histo1_%i_%i", i, j), "", resol_bin, resol_min, resol_max);

      resol_col_hist_2pad[i][j]  = new TH1F(Form("resol_histo_2pad%i_%i", i, j), "", resol_bin, resol_min, resol_max);
      resol_col_hist_1_2pad[i][j]  = new TH1F(Form("resol_histo1_2pad%i_%i", i, j), "", resol_bin, resol_min, resol_max);

      resol_col_hist_3pad[i][j]  = new TH1F(Form("resol_histo_3pad%i_%i", i, j), "", resol_bin, resol_min, resol_max);
      resol_col_hist_1_3pad[i][j]  = new TH1F(Form("resol_histo1_3pad%i_%i", i, j), "", resol_bin, resol_min, resol_max);
    }
    resol_col[i]          = new TH1F(Form("resol_%i", i), "", 36, 0., 36.);
    resol_col1[i]          = new TH1F(Form("resol1_%i", i), "", 36, 0., 36.);
    resol_colf[i]          = new TH1F(Form("resol_final_%i", i), "", 36, 0., 36.);

    resol_colf_2pad[i]          = new TH1F(Form("resol_final_2pad_%i", i), "", 36, 0., 36.);
    resol_colf_3pad[i]          = new TH1F(Form("resol_final_3pad_%i", i), "", 36, 0., 36.);

    mean_col[i]          = new TH1F(Form("mean_%i", i), "", 36, 0., 36.);
    mean_col1[i]          = new TH1F(Form("mean1_%i", i), "", 36, 0., 36.);
    mean_colf[i]          = new TH1F(Form("mean_final_%i", i), "", 36, 0., 36.);

    resol_colf_RMS[i]   = new TH1F(Form("resol_RMS_%i", i), "", 36, 0., 36.);
    resol_colf_FWHM[i]  = new TH1F(Form("resol_FWHM_%i", i), "", 36, 0., 36.);
  }

  vector< int > Entries_sel;

  //************************************************************
  //****************** LOOP OVER FILES  ************************
  //************************************************************
  TChain* geo_chain = new TChain("femGeomTree");
  TChain* data      = new TChain("padData");

  for (uint ifile=0;ifile< listOfFiles.size();ifile++){
    file = listOfFiles[ifile];
    data->AddFile(file);
    if (ifile == 0)
      geo_chain->AddFile(file);
  }
  vector<int> *iPad(0);
  vector<int> *jPad(0);
  vector<double> *xPad(0);
  vector<double> *yPad(0);

  vector<double> *dxPad(0);
  vector<double> *dyPad(0);

  geo_chain->SetBranchAddress("jPad", &jPad );
  geo_chain->SetBranchAddress("iPad", &iPad );

  geo_chain->SetBranchAddress("xPad", &xPad );
  geo_chain->SetBranchAddress("yPad", &yPad );

  geo_chain->SetBranchAddress("dxPad", &dxPad );
  geo_chain->SetBranchAddress("dyPad", &dyPad );
  geo_chain->GetEntry(0); // put into memory geometry info
  cout << "***************************" << endl;
  cout << "Reading channel mapping" << endl;
  cout << "jPad->size()  " << jPad->size() <<endl;
  cout << "iPad->size()  " << iPad->size() <<endl;

  cout << "Reading geometry" << endl;
  cout << "xPad->size()  " << xPad->size() <<endl;
  cout << "yPad->size()  " << yPad->size() <<endl;
  cout << "dxPad->size() " << dxPad->size() <<endl;
  cout << "dyPad->size() " << dyPad->size() <<endl;

  int Imax = -1;
  int Imin = 10000000;
  int Jmax = -1;
  int Jmin = 10000000;
  double Xmax = -1;
  double Xmin = 10000000;
  double Ymax = -1;
  double Ymin = 10000000;


  for (unsigned long i = 0; i < jPad->size(); ++i) {
    if (Imax < (*iPad)[i])
      Imax = (*iPad)[i];
    if (Imin > (*iPad)[i])
      Imin = (*iPad)[i];
    if (Jmax < (*jPad)[i])
      Jmax = (*jPad)[i];
    if (Jmin > (*jPad)[i])
      Jmin = (*jPad)[i];
    if (Xmax < (*xPad)[i])
      Xmax = (*xPad)[i];
    if (Xmin > (*xPad)[i])
      Xmin = (*xPad)[i];
    if (Ymax < (*yPad)[i])
      Ymax = (*yPad)[i];
    if (Ymin > (*yPad)[i])
      Ymin = (*yPad)[i];
  }

  cout << "***************************" << endl;
  cout << "J goes from " << Jmin << "\t to " << Jmax << endl;
  cout << "I goes from " << Imin << "\t to " << Imax << endl;
  cout << "X goes from " << Xmin << "\t to " << Xmax << endl;
  cout << "Y goes from " << Ymin << "\t to " << Ymax << endl;
  cout << "X and Y size are " << (*dxPad)[0] << "\t" << (*dyPad)[0] << endl;

  pair<Float_t, Float_t> geom[48][36];
  for (UInt_t it = 0; it < (*iPad).size(); ++it) {
    geom[(*jPad)[it]][(*iPad)[it]].first    = (*xPad)[it];
    geom[(*jPad)[it]][(*iPad)[it]].second   = (*yPad)[it];
  }

  gStyle->SetPalette(1);
  gStyle->SetOptStat(0);
  int Nevents=0;

  vector<short>          *listOfChannels(0);
  vector<vector<short> > *listOfSamples(0);

  data->SetBranchAddress("PadphysChannels", &listOfChannels );
  data->SetBranchAddress("PadADCvsTime"   , &listOfSamples );

  cout <<data->GetEntries() << " evts in file " << endl;

  if (Nevents<=0) Nevents=data->GetEntries();
  if (Nevents>data->GetEntries()) Nevents=data->GetEntries();

  Int_t total_events  = 0;
  Int_t reco_tracks   = 0;
  Int_t sel_tracks    = 0;

  for (Int_t iter = 0; iter < Niter; ++iter) {
    if (iter == 0)
      for (Int_t i = 0; i < Nevents; ++i)
        Entries_sel.push_back(i);

    Nevents = Entries_sel.size();

    cout << "[                                                  ] Nevents = " << Nevents << "\r[";
    uint ievt = 0;
    uint ievt_dot = 0;
    while (ievt != Entries_sel.size()) {
      if (ievt > ievt_dot) {
        cout <<"."<<flush;
        ievt_dot += Nevents/50;
        /*double vm, rss;
        process_mem_usage(vm, rss);
        cout << "Evt  " << ievt << " Ent  " <<  Entries_sel[ievt]  << "      VM: " << vm << "; RSS: " << rss << endl;*/
      }

      vector< vector<short> > PadDisplay;
      vector< vector<short> > PadTime;
      vector< vector<short> > PadDisplayNew;
      vector<vector< vector<short> > > MultiOutPad;
      vector<vector< vector<short> > > MultiOutPadTime;

      vector< vector< vector<short> > > PadDisplay3D;

      Nevents = Entries_sel.size();

      data->GetEntry(Entries_sel[ievt]);
      // clean for the next event
      PadDisplayNew.clear();
      //PadDisplay.clear();
      vector< vector<short> >().swap(PadDisplay);
      PadTime.clear();
      MultiOutPad.clear();

      if (!(*listOfSamples).size()) {
        Entries_sel.erase(Entries_sel.begin() + ievt);
        continue;
      }

      int time_bins = (*listOfSamples)[0].size();

      //PadDisplay3D.clear();
      vector< vector< vector<short> > >().swap(PadDisplay3D);
      PadDisplay3D.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z) {
        PadDisplay3D[z].resize(Imax+1);
        for (int t = 0; t <= Imax; ++t)
          PadDisplay3D[z][t].resize(time_bins, 0);
      }

      PadDisplay.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplay[z].resize(Imax+1, 0);

      PadTime.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadTime[z].resize(Imax+1, 0);

      PadDisplayNew.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplayNew[z].resize(Imax+1, 0);

      //************************************************************
      //*****************LOOP OVER CHANNELS ************************
      //************************************************************
      for (uint ic=0; ic< listOfChannels->size(); ic++){
        int chan= (*listOfChannels)[ic];
        // find out the maximum
        float adcmax=-1;
        Int_t it_t_max = -1;

        // one maximum per channel
        for (uint it = TIME_FIRST; it < TIME_LAST; it++){
        //for (uint it = 10; it < 450; it++){
          int adc= (*listOfSamples)[ic][it];
          PadDisplay3D[(*jPad)[chan]][(*iPad)[chan]][it] = adc;
          if (adc>adcmax && adc) {
            adcmax = adc;
            it_t_max = it;
          }
        }

        if (adcmax < 0) continue; // remove noise

        // noise suppression
        if (it_t_max < TIME_FIRST_REL || it_t_max > TIME_LAST_REL)
          continue;

        PadDisplay[(*jPad)[chan]][(*iPad)[chan]]  = adcmax;
        PadTime[(*jPad)[chan]][(*iPad)[chan]]     = it_t_max;
      } //loop over channels

      ++total_events;
#ifndef SEVERE_SUPPRESSION
      Int_t gErrorIgnoreLevel_bu = kSysError;
      if (SUPPRESS_ERRORS) {
        gErrorIgnoreLevel_bu = gErrorIgnoreLevel;
        gErrorIgnoreLevel = kSysError;
      }
#endif


      // apply the selection
#ifdef COSMIC
      if (!SelectionTrackCosmic(PadDisplay, PadTime, PadDisplayNew))
          continue;
#else

      //if (!TestSelection(PadDisplay, PadTime, PadDisplayNew, MultiOutPad)) {
      if (!TestSelection3D(PadDisplay, PadTime, PadDisplayNew, MultiOutPad, MultiOutPadTime, PadDisplay3D)) {
        Entries_sel.erase(Entries_sel.begin() + ievt);
        continue;
      }
#endif

#ifndef SEVERE_SUPPRESSION
      if (SUPPRESS_ERRORS)
        gErrorIgnoreLevel = gErrorIgnoreLevel_bu;
#endif

      for (uint trackId = 0; trackId < MultiOutPad.size(); ++trackId) {
        ++reco_tracks;

        // 1 - 70, 160
        // 2 - 120, 190
        // 3 - 220, 290
        // 10, 11 - 150, 230
        if (!TimeCut(MultiOutPad[trackId], MultiOutPadTime[trackId], 70., 160.))
          continue;

        ++sel_tracks;

        TGraphErrors* track = new TGraphErrors();
        TGraphErrors* track_m = new TGraphErrors();

        TGraphErrors* track_1[36];
        for (int i = 0; i < 36; ++i) {
          track_1[i] = new TGraphErrors();
        }

        int cluster[36];
        int cluster_N[36];
        double true_track[36];
        double cluster_mean[36];
        float charge_max[36];

        // At the moment ommit first and last column
        // first loop over column
        for (Int_t it_i = 1; it_i < Imax; ++it_i) {
          cluster[it_i]     = 0;
          cluster_N[it_i]   = 0;
          charge_max[it_i]  = 0;
          true_track[it_i]  = -999.;
          cluster_mean[it_i] = 0.;

          TH1F* cluster_h = new TH1F("cluster", "", 50,-17.5,17.5);

          for (Int_t it_j = 0; it_j <= Jmax; ++it_j) {
            if (!MultiOutPad[trackId][it_j][it_i])
              continue;

            cluster[it_i] += MultiOutPad[trackId][it_j][it_i];
            ++cluster_N[it_i];
            cluster_h->Fill(geom[it_j][0].second, MultiOutPad[trackId][it_j][it_i]);
            if (charge_max[it_i] < MultiOutPad[trackId][it_j][it_i]) {
              charge_max[it_i] = MultiOutPad[trackId][it_j][it_i];
            }
          }

          cluster_mean[it_i] = cluster_h->GetMean();

          delete cluster_h;

          if (!cluster[it_i] || !charge_max[it_i])
            continue;

          // minimise chi2 to extimate true_track
          double chi2_min     = 1e9;
          double scan_y       = cluster_mean[it_i] - scan_delta;

          if (iter > 0 && cluster_N[it_i] != 1) {
            for (Int_t scanId = 0; scanId < scan_Nsteps; ++scanId) {
              double chi2 = 0;
              for (Int_t it_j = 0; it_j <= Jmax; ++it_j) {
                if (!MultiOutPad[trackId][it_j][it_i])
                  continue;

                double a = 1. * MultiOutPad[trackId][it_j][it_i] / cluster[it_i];
                double center_pad_y = geom[it_j][0].second;
                double part;
                if (cluster_N[it_i] == 2)
                   part = (a - f2_2pad->Eval(scan_y - center_pad_y)) / (sqrt(MultiOutPad[trackId][it_j][it_i]) / cluster[it_i] );
                 else if (cluster_N[it_i] == 3)
                  part = (a - f2_3pad->Eval(scan_y - center_pad_y)) / (sqrt(MultiOutPad[trackId][it_j][it_i]) / cluster[it_i] );
                else
                  part = (a - function->Eval(scan_y - center_pad_y)) / (sqrt(MultiOutPad[trackId][it_j][it_i]) / cluster[it_i] );

                chi2 += part * part;
              }

              if (chi2 < chi2_min) {
                chi2_min = chi2;
                true_track[it_i] = scan_y;
              }

              scan_y += scan_step;
            }
          }

          double x = geom[0][it_i].first;

          if (iter == 0 || cluster_N[it_i] == 1)
            true_track[it_i] = cluster_mean[it_i];

          track->SetPoint(track->GetN(), x, true_track[it_i]);
          for (int i = 1; i < Imax; ++i) {
            if (i != it_i)
              track_1[i]->SetPoint(track_1[i]->GetN(), x, true_track[it_i]);
          }

          track_m->SetPoint(track_m->GetN(), x, cluster_mean[it_i]);
          double error;
          if (cluster_N[it_i] == 1)
            error = 0.2;
          else {
            if (iter == 0)
              error = 0.1;
            else
              error = uncertainty;
          }
          track->SetPointError(track->GetN()-1, 0., error);
          for (int i = 1; i < Imax; ++i) {
            if (i != it_i)
              track_1[i]->SetPointError(track_1[i]->GetN() - 1, 0., error);
          }
        } // loop over i

        track->Fit("pol1", "Q");
        TF1* fit = track->GetFunction("pol1");

        //track_m->Fit("pol1", "Q");
        //TF1* fit1 = track_m->GetFunction("pol1");

        if (!fit)
          continue;

        double quality = fit->GetChisquare() / fit->GetNDF();
        double k = fit->GetParameter(1);
        double b = fit->GetParameter(0);

        Chi2_track[iter]->Fill(quality);

        double k1[36];
        double b1[36];

        for (int i = 1; i < Imax; ++i) {
          track_1[i]->Fit("pol1", "Q");
          TF1* fit1 = track_1[i]->GetFunction("pol1");
          k1[i] = fit1->GetParameter(1);
          b1[i] = fit1->GetParameter(0);
        }

        // second loop over columns
        for (Int_t it_i = 1; it_i < Imax; ++it_i) {

          if (true_track[it_i]  == -999.)
            continue;

          double x    = geom[0][it_i].first;
          double track_fit_y  = k * x + b;
          double track_fit_y1 = k1[it_i] * x + b1[it_i];
          //double track_fit_y1 = k1 * x + b1;

          // fill SR
          resol_col_hist[iter][it_i]->Fill(true_track[it_i] - track_fit_y);
          resol_col_hist_1[iter][it_i]->Fill(true_track[it_i] - track_fit_y1);

          if (cluster_N[it_i] == 2) {
            resol_col_hist_2pad[iter][it_i]->Fill(true_track[it_i] - track_fit_y);
            resol_col_hist_1_2pad[iter][it_i]->Fill(true_track[it_i] - track_fit_y1);
          } else if (cluster_N[it_i] == 3) {
            resol_col_hist_3pad[iter][it_i]->Fill(true_track[it_i] - track_fit_y);
            resol_col_hist_1_3pad[iter][it_i]->Fill(true_track[it_i] - track_fit_y1);
          }

          if (cluster_N[it_i] == 1)
            continue;
          // Fill PRF
          for (Int_t it_j = 0; it_j <= Jmax; ++it_j) {
            if (!cluster[it_i] || !MultiOutPad[trackId][it_j][it_i])
              continue;

            double charge = 1. * MultiOutPad[trackId][it_j][it_i] / cluster[it_i];
            double center_pad_y = geom[it_j][0].second;

            // fill PRF
            PRF_H[iter]->Fill(center_pad_y - track_fit_y, charge);
            PRF_sc[iter]->SetPoint(PRF_sc[iter]->GetN(), center_pad_y - track_fit_y, charge);

            if (cluster_N[it_i] == 2)
              PRF_H_2pad[iter]->Fill(center_pad_y - track_fit_y, charge);
            else if (cluster_N[it_i] == 3)
              PRF_H_3pad[iter]->Fill(center_pad_y - track_fit_y, charge);
            else if (cluster_N[it_i] == 4)
              PRF_H_4pad[iter]->Fill(center_pad_y - track_fit_y, charge);
          }
        } // loop over colums
        delete track;
        delete track_m;
        for (int i = 0; i < 36; ++i) {
          delete track_1[i];
        }
      } // loop over tracks
      ++ievt;
    } // loop over events
    cout << "]" << endl;

    out_file->cd();
    gStyle->SetOptFit(1);

    cout << "Total events number     : " << total_events << endl;
    cout << "Reco tracks             : " << reco_tracks << endl;
    cout << "Selected tracks         : " << sel_tracks << endl;

    cout << "iter " << iter << endl;

    for (Int_t i = 1; i < PRF_H[iter]->GetXaxis()->GetNbins(); ++i) {

      TH1D* temp_h = PRF_H[iter]-> ProjectionY(Form("projections_%i_%i", iter, i), i, i);

      double x = PRF_H[iter]->GetXaxis()->GetBinCenter(i);
      double y = temp_h->GetBinCenter(temp_h->GetMaximumBin());

      float start = -1.;
      float end   = -1.;
      float max = temp_h->GetMaximum();

      for (Int_t bin = 0; bin < temp_h->GetXaxis()->GetNbins(); ++bin) {
        if (start == -1. && temp_h->GetBinContent(bin) >= max / 2.)
          start = temp_h->GetBinCenter(bin);

        if (end == -1. && start != -1. && temp_h->GetBinContent(bin) <= max / 2.)
          end = temp_h->GetBinCenter(bin);
      }

      float e = end - start;

      PRF_gr[iter]->SetPoint(PRF_gr[iter]->GetN(), x, y);
      PRF_gr[iter]->SetPointError(PRF_gr[iter]->GetN()-1, 0, e/2.);
    }

    // 2 pad
    for (Int_t i = 1; i < PRF_H_2pad[iter]->GetXaxis()->GetNbins(); ++i) {

      TH1D* temp_h = PRF_H_2pad[iter]-> ProjectionY(Form("projections_%i_%i", iter, i), i, i);

      double x = PRF_H_2pad[iter]->GetXaxis()->GetBinCenter(i);
      double y = temp_h->GetBinCenter(temp_h->GetMaximumBin());

      float start = -1.;
      float end   = -1.;
      float max = temp_h->GetMaximum();

      for (Int_t bin = 0; bin < temp_h->GetXaxis()->GetNbins(); ++bin) {
        if (start == -1. && temp_h->GetBinContent(bin) >= max / 2.)
          start = temp_h->GetBinCenter(bin);

        if (end == -1. && start != -1. && temp_h->GetBinContent(bin) <= max / 2.)
          end = temp_h->GetBinCenter(bin);
      }

      float e = end - start;

      PRF_gr_2pad[iter]->SetPoint(PRF_gr_2pad[iter]->GetN(), x, y);
      PRF_gr_2pad[iter]->SetPointError(PRF_gr_2pad[iter]->GetN()-1, 0, e/2.);
    }

    // 3 pad

    for (Int_t i = 1; i < PRF_H_3pad[iter]->GetXaxis()->GetNbins(); ++i) {

      TH1D* temp_h = PRF_H_3pad[iter]-> ProjectionY(Form("projections_%i_%i", iter, i), i, i);

      double x = PRF_H_3pad[iter]->GetXaxis()->GetBinCenter(i);
      double y = temp_h->GetBinCenter(temp_h->GetMaximumBin());

      float start = -1.;
      float end   = -1.;
      float max = temp_h->GetMaximum();

      for (Int_t bin = 0; bin < temp_h->GetXaxis()->GetNbins(); ++bin) {
        if (start == -1. && temp_h->GetBinContent(bin) >= max / 2.)
          start = temp_h->GetBinCenter(bin);

        if (end == -1. && start != -1. && temp_h->GetBinContent(bin) <= max / 2.)
          end = temp_h->GetBinCenter(bin);
      }

      float e = end - start;

      PRF_gr_3pad[iter]->SetPoint(PRF_gr_3pad[iter]->GetN(), x, y);
      PRF_gr_3pad[iter]->SetPointError(PRF_gr_3pad[iter]->GetN()-1, 0, e/2.);
    }

    PRF_gr[iter]->Fit(function_str, "", "", -1.2, 1.2);
    TF1* fit0 = PRF_gr[iter]->GetFunction(function_str);
    cout << "Chi2   " << fit0->GetChisquare() << endl;
    cout << "NDOF   " << fit0->GetNDF() << endl;
    cout << "Qual   " << fit0->GetChisquare() / fit0->GetNDF() << endl;

    // 2 pad
    PRF_gr_2pad[iter]->Fit("f2_2pad", "", "", -0.7, 0.7);
    TF1* fit0_2pad = PRF_gr_2pad[iter]->GetFunction("f2_2pad");
    cout << "Chi2   " << fit0_2pad->GetChisquare() << endl;
    cout << "NDOF   " << fit0_2pad->GetNDF() << endl;
    cout << "Qual   " << fit0_2pad->GetChisquare() / fit0_2pad->GetNDF() << endl;

    // 3 pad
    PRF_gr_3pad[iter]->Fit("f2_3pad", "", "", -1.2, 1.2);
    TF1* fit0_3pad = PRF_gr_3pad[iter]->GetFunction("f2_3pad");
    cout << "Chi2   " << fit0_3pad->GetChisquare() << endl;
    cout << "NDOF   " << fit0_3pad->GetNDF() << endl;
    cout << "Qual   " << fit0_3pad->GetChisquare() / fit0_3pad->GetNDF() << endl;

    uncertainty = 0.;

    dir->cd();

    for (Int_t i = 1; i < 35; ++i) {
      c5->cd();
      gStyle->SetOptFit(1);
      resol_col_hist[iter][i]->Fit("gaus", "Q");
      resol_col_hist[iter][i]->Write();
      resol_col_hist_1[iter][i]->Fit("gaus", "Q");
      //resol_col_hist_1[iter][i]->Write();

      resol_col_hist_2pad[iter][i]->Fit("gaus", "Q");
      resol_col_hist_1_2pad[iter][i]->Fit("gaus", "Q");
      resol_col_hist_2pad[iter][i]->Write();

      resol_col_hist_3pad[iter][i]->Fit("gaus", "Q");
      resol_col_hist_1_3pad[iter][i]->Fit("gaus", "Q");
      resol_col_hist_3pad[iter][i]->Write();

      double mean = resol_col_hist[iter][i]->GetFunction("gaus")->GetParameter(1);
      double mean1 = resol_col_hist_1[iter][i]->GetFunction("gaus")->GetParameter(1);
      double sigma = resol_col_hist[iter][i]->GetFunction("gaus")->GetParameter(2);
      double sigma1 = resol_col_hist_1[iter][i]->GetFunction("gaus")->GetParameter(2);

      double RMS  = resol_col_hist[iter][i]->GetRMS();
      double RMS1 = resol_col_hist_1[iter][i]->GetRMS();

      Int_t fb = resol_col_hist[iter][i]->FindFirstBinAbove(0.5 * resol_col_hist[iter][i]->GetMaximum());
      Int_t lb = resol_col_hist[iter][i]->FindLastBinAbove(0.5 * resol_col_hist[iter][i]->GetMaximum());
      double FWHM   = 0.5 * (resol_col_hist[iter][i]->GetBinLowEdge(lb) + resol_col_hist[iter][i]->GetBinWidth(lb) - resol_col_hist[iter][i]->GetBinLowEdge(fb));

      Int_t fb1 = resol_col_hist_1[iter][i]->FindFirstBinAbove(0.5 * resol_col_hist_1[iter][i]->GetMaximum());
      Int_t lb1 = resol_col_hist_1[iter][i]->FindLastBinAbove(0.5 * resol_col_hist_1[iter][i]->GetMaximum());
      double FWHM1  = 0.5 * (resol_col_hist_1[iter][i]->GetBinLowEdge(lb1) + resol_col_hist_1[iter][i]->GetBinWidth(lb1) - resol_col_hist_1[iter][i]->GetBinLowEdge(fb1));

      mean_col[iter]->SetBinContent(i+1, mean);
      mean_col1[iter]->SetBinContent(i+1, mean1);
      mean_colf[iter]->SetBinContent(i+1, sqrt(mean * mean1));

      resol_col[iter]->SetBinContent(i+1, sigma);
      resol_col1[iter]->SetBinContent(i+1, sigma1);
      resol_colf[iter]->SetBinContent(i+1, sqrt(sigma * sigma1));
      resol_colf_RMS[iter]->SetBinContent(i+1, sqrt(RMS * RMS1));
      resol_colf_FWHM[iter]->SetBinContent(i+1, sqrt(FWHM * FWHM1));
      uncertainty += sqrt(sigma * sigma1) / 34;

      sigma = resol_col_hist_2pad[iter][i]->GetFunction("gaus")->GetParameter(2);
      sigma1 = resol_col_hist_1_2pad[iter][i]->GetFunction("gaus")->GetParameter(2);
      resol_colf_2pad[iter]->SetBinContent(i+1, sqrt(sigma * sigma1));

      sigma = resol_col_hist_3pad[iter][i]->GetFunction("gaus")->GetParameter(2);
      sigma1 = resol_col_hist_1_3pad[iter][i]->GetFunction("gaus")->GetParameter(2);
      resol_colf_3pad[iter]->SetBinContent(i+1, sqrt(sigma * sigma1));
    }

    float RMS = 0;
    for (int i = 2; i <= resol_colf[iter]->GetXaxis()->GetNbins() - 1; ++i) {
      RMS += (resol_colf[iter]->GetBinContent(i) - uncertainty) * (resol_colf[iter]->GetBinContent(i) - uncertainty);
    }
    RMS /= 34;
    RMS = sqrt(RMS);

    out_file->cd();

    cout << "Avarage uncertainty    " << uncertainty * 10000 << " um   RMS " <<  RMS * 10000. << " um " << endl;

    c1->cd();
    PRF_sc[iter]->Draw("ap");
    PRF_sc[iter]->GetXaxis()->SetRangeUser(-1.5, 1.5);
    c1->Modified();
    c1->Update();
    c1->Print((prefix + "/prf_sc.pdf").Data());

    c3->cd();
    PRF_H[iter]->Draw("colz");
    c3->Modified();
    c3->Update();
    c3->Print((prefix + "/prf_h.pdf").Data());

    c2->cd();
    PRF_gr[iter]->Draw("ap");
    c2->Modified();
    c2->Update();
    c2->Print((prefix + "/prf_gr.pdf").Data());

    c4->cd();
    resol_col[iter]->Draw();

    //resol_col[iter]->Write();
    //resol_col1[iter]->Write();
    resol_colf[iter]->Write();
    resol_colf_RMS[iter]->Write();
    resol_colf_FWHM[iter]->Write();

    resol_colf_2pad[iter]->Write();
    resol_colf_3pad[iter]->Write();

    mean_col[iter]->Write();
    mean_col1[iter]->Write();
    mean_colf[iter]->Write();

    Chi2_track[iter]->Write();
    PRF_sc[iter]->Write();
    PRF_H[iter]->Write();
    PRF_gr[iter]->Write();
    PRF_gr_2pad[iter]->Write();
    PRF_gr_3pad[iter]->Write();

    PRF_H_2pad[iter]->Write();
    PRF_H_3pad[iter]->Write();
    PRF_H_4pad[iter]->Write();

    if (!fit0) {
      cout << "Fitting failed" << endl;
      return;
    }

    for (int i = 0; i < fit0->GetNumberFreeParameters(); ++i) {
      double par = fit0->GetParameter(i);
      function->SetParameter(i, par);

      par = fit0_2pad->GetParameter(i);
      f2_2pad->SetParameter(i, par);

      par = fit0_3pad->GetParameter(i);
      f2_3pad->SetParameter(i, par);
    }


  } // loop over iterations

  cout << "Done" << endl;
  out_file->Close();
  exit(1);
}

