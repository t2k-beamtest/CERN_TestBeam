#define THIS_NAME ana_dEdx
#define NOINTERACTIVE_OUTPUT
#define OVERRIDE_OPTIONS

#include "ilc/common_header.h"
#include "selection.C"

#include <vector>

//#define COSMIC
//#define PEAK2
//#define TWOSIDE

//#define SEVERE_SUPPRESSION
bool SUPPRESS_ERRORS = true;

void ana_dEdx() {

#ifdef SEVERE_SUPPRESSION
  gErrorIgnoreLevel = kSysError;
#endif

  bool PLOT = false;

  vector<TString> listOfFiles;
  TString file    = "default.root";
  TString prefix  = "dEdx/";
  for (int iarg=0; iarg<gApplication->Argc(); iarg++){
    if (string( gApplication->Argv(iarg))=="-f" || string( gApplication->Argv(iarg))=="--file" ){
      iarg++;
      file = gApplication->Argv(iarg);
      if (file.Contains(".root")) {

        cout << "adding filename" <<" " << file << endl;
        listOfFiles.push_back(file);
      } else {
        fstream fList(file);
        if (fList.good()) {
          while (fList.good()) {
            string filename;
            getline(fList, filename);
            if (fList.eof()) break;
            listOfFiles.push_back(filename);
          }
        }
      }
    } else if (string( gApplication->Argv(iarg))=="-o" || string( gApplication->Argv(iarg))=="--output" ){
      iarg++;
      prefix = gApplication->Argv(iarg);
    } else if (string( gApplication->Argv(iarg))=="-h" || string( gApplication->Argv(iarg))=="--help" ){
      cout << "**************************************" << endl;
      cout << "Macros run options:" << endl;
      cout << "   -f || --file      input file (*.root) or filelist" << endl;
      cout << "   -o || --output    output folder for plots and out file" << endl;
      cout << "   -h || --help      print help info" << endl;
      cout << "**************************************" << endl;
    }
  }

#if TWOSIDE && PEAK2
  cout << "You can not fit 2-side Gaussian and 2 Gaussian peaks at the same moment." << endl;
  cout << "Exit." << endl;
  exit(1);
#endif

  cout<<"opening canvas"<<endl;



  TTree *treeInfo = new TTree("treeInfo","tracks info");
  int ev;
  int n_cluster;
  float x,y,z;
  float Ct[10];
  float charge_tot;

  float cl_charge[36];
  int cl_pads[36];
  
  treeInfo->Branch("ev",&ev,"ev/I");
  treeInfo->Branch("x",&x,"x/F");
  treeInfo->Branch("y",&y,"y/F");
  treeInfo->Branch("z",&z,"z/F");
  treeInfo->Branch("Ct",&Ct,"Ct[10]/F");
  treeInfo->Branch("charge_tot",&charge_tot);
  treeInfo->Branch("cl_charge",&cl_charge,"cl_charge[36]/F");
  treeInfo->Branch("cl_pads",&cl_pads,"cl_pads[36]/I");
  treeInfo->Branch("n_cluster",&n_cluster);


  
  TCanvas *c1 = new TCanvas("c1","evadc1",0, 0, 400,300);
  TCanvas *c2 = new TCanvas("c2","evadc2",400, 0, 400,300);
  TCanvas *c3 = new TCanvas("c3","evadc3",800, 0, 400,300);
  TCanvas *c4 = new TCanvas("c4","evadc4", 0, 400, 400,300);
  TCanvas *c5 = new TCanvas("c5","evadc5", 400, 400, 400,300);

  int thr  = 0.;
  double alpha[10] = {0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.9,1.0};

  TH2F* PadDisplayGeo=new TH2F("PadDisplayGeo","X vs Y of hits",50,-17.15,17.15, 38,-18.13,18.13);
  TH2F* PadDisplayH=new TH2F("PadDisplay1","I vs J of hits",38,-1.,37.,50,-1.,49.);
  TH2F* PadDisplayH1=new TH2F("PadDisplay2","I vs J of hits",38,-1.,37.,50,-1.,49.);

  TH1F* ClusterCharge     = new TH1F("cluster_charge","Cluster charge",150,0,3000);
  TH1F* ClusterChargeCur  = new TH1F("cluster_chargeC","Cluster charge current",150,0,3000);
  TH1F* ClusterNormCharge = new TH1F("cluster_norm_charge","Truncated mean energy deposit",150,0,1000);

  TH1F* TimeMaximum       = new TH1F("time_max", "time of the maximum", 530, 0., 530.);
  TH1F* TimeMaximumL      = new TH1F("time_maxL", "time of the maximum", 100, 0., 530.);
  TH1F* TimeMaximumT      = new TH1F("time_maxT", "time of the maximum", 530, 0., 530.);

  TH2F* PadDisplayHIT     = new TH2F("PadDisplayH","I vs J of hits",38,-1.,37.,50,-1.,49.);
  TH2F* PadDisplayMAX     = new TH2F("PadDisplayM","I vs J of hits",38,-1.,37.,50,-1.,49.);

  TH1F* PadsPerCluster    = new TH1F("PadsPerCluster", "ppc", 8, 0., 8.);

  TH1F* QvsT  = new TH1F("QvsT", "", 520, 0., 520.);
  TH1F* QvsTN = new TH1F("QvsTN", "", 520, 0., 520.);

  TH2F* ChargeCorrelation = new TH2F("corralation", "Charge correlation", 150,0,3000, 150,0,3000);

  Int_t total_events = 0;
  Int_t sel_events = 0;

  vector< vector<int> > PadDisplay;
  vector< vector<int> > PadTime;
  vector< vector<int> > PadDisplayNew;
  vector< vector<int> > PadIntegral;
  vector< vector<vector<short> > > PadDisplayV;

  //************************************************************
  //****************** LOOP OVER FILES  ************************
  //************************************************************
  vector< vector<int > > EventClusters;

  TFile*f=0;

  cout<<"opening files "<<listOfFiles.size()<<endl;
  
  for (uint ifile=0;ifile< listOfFiles.size();ifile++){
    file = listOfFiles[ifile];
    if (f!=0) f->Close();
    cout << "opening file " << file <<  endl;
    f= new TFile(file);
    if (!f->IsOpen()) {
      cout << " WARNING problem in opening file " << file << endl;
      return;
    }

    vector<int> *iPad(0);
    vector<int> *jPad(0);
    vector<double> *xPad(0);
    vector<double> *yPad(0);

    vector<double> *dxPad(0);
    vector<double> *dyPad(0);

    TTree * tgeom= (TTree*) f->Get("femGeomTree");
    tgeom->SetBranchAddress("jPad", &jPad );
    tgeom->SetBranchAddress("iPad", &iPad );

    tgeom->SetBranchAddress("xPad", &xPad );
    tgeom->SetBranchAddress("yPad", &yPad );

    tgeom->SetBranchAddress("dxPad", &dxPad );
    tgeom->SetBranchAddress("dyPad", &dyPad );
    tgeom->GetEntry(0); // put into memory geometry info
    cout << "Reading channel mapping" << endl;
    cout << "jPad->size()  " << jPad->size() <<endl;
    cout << "iPad->size()  " << iPad->size() <<endl;

    cout << "Reading geometry" << endl;
    cout << "xPad->size()  " << xPad->size() <<endl;
    cout << "yPad->size()  " << yPad->size() <<endl;
    cout << "dxPad->size() " << dxPad->size() <<endl;
    cout << "dyPad->size() " << dyPad->size() <<endl;

    int Imax = -1;
    int Imin = 10000000;
    int Jmax = -1;
    int Jmin = 10000000;

    for (unsigned long i = 0; i < jPad->size(); ++i) {
      if (Imax < (*iPad)[i])
        Imax = (*iPad)[i];
      if (Imin > (*iPad)[i])
        Imin = (*iPad)[i];
      if (Jmax < (*jPad)[i])
        Jmax = (*jPad)[i];
      if (Jmin > (*jPad)[i])
        Jmin = (*jPad)[i];
    }

    gStyle->SetPalette(1);
    gStyle->SetOptStat(0);
    int Nevents=0;
    TTree *t = (TTree*) f->Get("padData");
    cout <<t->GetEntries() << " evts in file " << endl;

    vector<short>          *listOfChannels(0);
    vector<vector<short> > *listOfSamples(0);

    t->SetBranchAddress("PadphysChannels", &listOfChannels );
    t->SetBranchAddress("PadADCvsTime"   , &listOfSamples );

  
    if (Nevents<=0) Nevents=t->GetEntries();
    if (Nevents>t->GetEntries()) Nevents=t->GetEntries();

    cout << "[          ] Nevents = "<<Nevents<<"\r[";

    //************************************************************
    //****************** LOOP OVER EVENTS ************************
    //************************************************************
    for (int ievt=0; ievt < Nevents ; ievt++){
      if (ievt%(Nevents/10)==0)
        cout <<"."<<flush;


      //if(ievt>1000)
      //break;

      //cout<<"eventi "<<ievt<<" "<<listOfChannels->size()<<endl;
      
      t->GetEntry(ievt);
      PadDisplayH->Reset();
      PadDisplayH1->Reset();

      // clean for the next event
      PadDisplayGeo->Reset();
      PadDisplayNew.clear();
      PadDisplay.clear();
      PadIntegral.clear();
      PadTime.clear();

      PadDisplay.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplay[z].resize(Imax+1, 0);

      PadTime.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadTime[z].resize(Imax+1, 0);

      PadDisplayNew.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplayNew[z].resize(Imax+1, 0);

      PadIntegral.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadIntegral[z].resize(Imax+1, 0);

      PadDisplayV.resize(Jmax+1);
      for (int z=0; z <= Jmax; ++z)
        PadDisplayV[z].resize(Imax+1);

      ClusterChargeCur->Reset();


      //************************************************************
      //*****************LOOP OVER CHANNELS ************************
      //************************************************************
      for (uint ic=0; ic< listOfChannels->size(); ic++){


        int chan= (*listOfChannels)[ic];
        // find out the maximum
        float adcmax=-1;
        Int_t it_max = -1;

        // one maximum per channel
        for (uint it = 0; it < (*listOfSamples)[ic].size(); it++){
        //for (uint it = 10; it < 450; it++){
          int adc= (*listOfSamples)[ic][it];
          if (adc>adcmax && adc > thr) {
            adcmax = adc;
            it_max = it;
          }
        }

        int lower = max (it_max - 10, 5);
        int upper = min(it_max + 20, 490);
        int integral = 0;
        for (int it = lower; it < upper; it++){
          // stop integrating if noise
          if ((*listOfSamples)[ic][it] < 0) {
            if (it > it_max)
              break;
            else continue;
          }

          integral += (*listOfSamples)[ic][it];
        }



        // PadDisplayV[(*jPad)[chan]][(*iPad)[chan]] = (*listOfSamples)[ic];

        if (adcmax<0) continue; // remove noise
        //if ((*iPad)[chan] == Imin || (*iPad)[chan] == Imax ||
        //    (*jPad)[chan] == Jmin || (*jPad)[chan] == Jmax)
        //    continue;

        PadDisplay[(*jPad)[chan]][(*iPad)[chan]]  = adcmax;
        PadTime[(*jPad)[chan]][(*iPad)[chan]]     = it_max;
        PadIntegral[(*jPad)[chan]][(*iPad)[chan]] = integral;

      }//loop over channels
      ++total_events;

      
      
#ifndef SEVERE_SUPPRESSION
      Int_t gErrorIgnoreLevel_bu = kSysError;
      if (SUPPRESS_ERRORS) {
        gErrorIgnoreLevel_bu = gErrorIgnoreLevel;
        gErrorIgnoreLevel = kSysError;
      }
#endif


      // apply the selection
#ifdef COSMIC
      //if (!SelectionTrackCosmic(PadDisplay, PadTime, PadDisplayNew))
      if (!TestSelection3D(PadDisplay, PadTime, PadDisplayNew))
	continue;
#else
      if (!SelectionTrackBeam(PadDisplay, PadTime, PadDisplayNew))
          continue;
#endif

#ifndef SEVERE_SUPPRESSION
      if (SUPPRESS_ERRORS)
        gErrorIgnoreLevel = gErrorIgnoreLevel_bu;
#endif

      ++sel_events;

  
      ev = ievt;
      
      for (int j = 0; j < 47; ++j) {
        for (int i = 0; i < 36; ++i) {
          if (!PadDisplayNew[j][i])
            continue;
          TimeMaximum->Fill(PadTime[j][i]);
          TimeMaximumL->Fill(PadTime[j][i]);
        }
      }


      // charge per cluster study
      vector <Float_t> cluster_charge;
      vector <Float_t> cluster_pads;
      cluster_charge.clear();
      cluster_pads.clear();
      int offset = 1;

      for(int i=0;i<36;i++){
	cl_charge[i]=0;
	cl_pads[i]=0;
      }
      int count =0;

      for (Int_t it_i = offset; it_i < Imax - offset ; ++it_i) {
	
        Int_t cluster = 0;
        Int_t pads_per_cluster = 0;
	
        for (Int_t it_j = 0; it_j < Jmax; ++it_j) {
	  
          cluster += PadDisplayNew[it_j][it_i];
          if (PadDisplayNew[it_j][it_i] > 0) {
            ++pads_per_cluster;
            PadDisplayHIT->Fill(it_i, it_j, 1);
	    if(it_i==0){
	      x = it_i;
	      y = it_j;
	      z = PadTime[it_j][it_i];
	    }
          }
          PadDisplayMAX->Fill(it_i, it_j, PadDisplayNew[it_j][it_i]);
          int integral = PadIntegral[it_j][it_i];
          QvsT->Fill(PadTime[it_j][it_i], integral);
          QvsTN->Fill(PadTime[it_j][it_i]);
        }
	
	if (cluster != 0) {
	  PadsPerCluster->Fill(pads_per_cluster);
	  ClusterCharge->Fill(cluster);
	  ClusterChargeCur->Fill(cluster);
	  cluster_charge.push_back(cluster);
	  cluster_pads.push_back(pads_per_cluster);
	  //cl_charge->push_back(cluster);
	  //cl_pads->push_back(pads_per_cluster);

	  cl_charge[count]=cluster;
	  cl_pads[count]=pads_per_cluster;
	  count++;
	  
	  UInt_t clusterID = cluster_charge.size();
	  
	  if (clusterID != 1)
	    ChargeCorrelation->Fill(cluster_charge[clusterID-1], cluster_charge[clusterID-2]);
	}

      }
      	
    
      sort(cluster_charge.begin(), cluster_charge.end());


      
      
      for(int k=0;k<10;k++){
	Float_t norm_cluster = 0.;
        Int_t i_max = round(alpha[k] * cluster_charge.size());
        n_cluster = cluster_charge.size();
        for (int i = 0; i < std::min(i_max, int(cluster_charge.size())); ++i)
          norm_cluster += cluster_charge[i];

        charge_tot = norm_cluster;

	//cout<<"before "<<norm_cluster<<" "<<alpha[k]*cluster_charge.size()<<endl;

        norm_cluster *= 1 / (alpha[k] * cluster_charge.size());

        if(k==7)ClusterNormCharge->Fill(norm_cluster);
	//cout<<k<<" "<<alpha[k]<<" "<<i_max<<" "<<norm_cluster<<endl;

        Ct[k] = norm_cluster;
      }

      treeInfo->Fill();
      
    } // loop over events
    cout << endl;
  } // loop over files

  gStyle->SetOptStat("RMne");
  TFile* out_file = new TFile((prefix + "output.root").Data(), "RECREATE");
  //TFile* out_file = new TFile((prefix + ).Data(), "RECREATE");


  treeInfo->Write();
  
  c1->cd();

  ClusterCharge->Draw();
  ClusterCharge->Write();
  c1->Print((prefix+"ClusterCharge_adv.pdf").Data());

#ifdef PEAK2
  TF1* f1 = new TF1("f1", "[0] * TMath::Gaus(x, [1], [2]) + [3] * TMath::Gaus(x, [4], [5])", 0, 1300);
#elif TWOSIDE
  TF1* f1 = new TF1("f1", "( (x < [1] ) ? [0] * TMath::Gaus(x, [1], [2]) : [0] * TMath::Gaus(x, [1], [3]))", 0, 1300);
#else
  TF1* f1 = new TF1("f1", "([0] * TMath::Gaus(x, [1], [2]))", 0, 1300);
#endif

#ifdef COSMIC
  f1->SetParameters(2500, 200, 50, 50);
#else
#ifdef PEAK2
  f1->SetParameters(200, 300, 50, 300, 400, 50);
#elif TWOSIDE
  f1->SetParameters(200, 700, 50, 50);
#else
  f1->SetParameters(900, 500, 30);
#endif
#endif
  f1->SetParName(0, "Const");

#ifdef PEAK2
  f1->SetParName(0, "Const1");
  f1->SetParName(1, "Mean1");
  f1->SetParName(2, "Sigma1");
  f1->SetParName(3, "Const2");
  f1->SetParName(4, "Mean2");
  f1->SetParName(5, "Sigma2");
#elif TWOSIDE
  f1->SetParName(1, "Mean");
  f1->SetParName(2, "Left sigma");
  f1->SetParName(3, "Right sigma");
#else
  f1->SetParName(1, "Mean");
  f1->SetParName(2, "Sigma");
#endif

  c2->cd();
  TH1F* ClusterNormChargeNF = (TH1F*)ClusterNormCharge->Clone("norm_charge_no_fit");
  ClusterNormChargeNF->Write();
  ClusterNormCharge->Fit("f1");

  gStyle->SetOptFit(1);

  ClusterNormCharge->Draw();
  gPad->Update();
  ClusterNormCharge->Write();
  c2->Print((prefix+"ClusterChargeNorm_adv.pdf").Data());

  gStyle->SetOptFit(0);

  c3->cd();
  TimeMaximum->Draw();
  TimeMaximum->Write();
  c3->Print((prefix+"TimeMaximum_adv.pdf").Data());
  c4->cd();
  TimeMaximumL->Draw();
  c4->Print((prefix+"TimeMaximumL_adv.pdf").Data());
  TimeMaximumT->Draw();
  c4->Print((prefix+"TimeMaximumT_adv.pdf").Data());

  gStyle->SetOptStat(0);

  c4->cd();
  PadDisplayHIT->Draw("colz");
  PadDisplayHIT->Write();
  c4->Print((prefix+"pad_scan_hit_track_adv.pdf").Data());
  PadDisplayMAX->Draw("colz");
  PadDisplayMAX->Write();
  c4->Print((prefix+"pad_scan_max_track_adv.pdf").Data());


  PadsPerCluster->Draw();
  PadsPerCluster->Write();
  c4->Print((prefix+"PadsPerCluster_adv.pdf").Data());

  TH2F* PadDisplayMAX2 = (TH2F*)PadDisplayMAX->Clone("avg_charge");
  PadDisplayMAX2->Divide(PadDisplayHIT);
  PadDisplayMAX2->Draw("colz");
  PadDisplayMAX2->Write();
  c4->Print((prefix+"AvChargePerHit_adv.pdf").Data());

  c5->cd();
  QvsT->Divide(QvsTN);
  QvsT->Draw();
  QvsT->Write();
  c5->Print((prefix+"avgQvsT.pdf").Data());

  ChargeCorrelation->Draw("colz");
  ChargeCorrelation->Write();
  c5->Print((prefix+"ChargeCorrelation.pdf").Data());

  out_file->Close();

  cout << "Total events number     : " << total_events << endl;
  //cout << "Not empty events number : " << filled_events << endl;
  cout << "Track events number     : " << sel_events << endl;

  return;
}
