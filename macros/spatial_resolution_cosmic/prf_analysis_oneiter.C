#define THIS_NAME prf_analysis
#define NOINTERACTIVE_OUTPUT
#define OVERRIDE_OPTIONS

#include "../ilc/common_header.h"
#include "../selection.C"
#include "../selection/selection3D.C"
#include "../selection/selection3D_cosmic.C"
#include "../selection/selection_ana.C"
#include "../selection/DBSCAN.C"

#include <iostream>
#include <fstream>
#include <unistd.h>

//#define COSMIC

//#define SEVERE_SUPPRESSION
bool SUPPRESS_ERRORS = true;

void process_mem_usage(double& vm_usage, double& resident_set)
{
    vm_usage     = 0.0;
    resident_set = 0.0;

    // the two fields we want
    unsigned long vsize;
    long rss;
    {
        std::string ignore;
        std::ifstream ifs("/proc/self/stat", std::ios_base::in);
        ifs >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
                >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore >> ignore
                >> ignore >> ignore >> vsize >> rss;
    }

    long page_size_kb = sysconf(_SC_PAGE_SIZE) / 1024; // in case x86-64 is configured to use 2MB pages
    vm_usage = vsize / 1024.0;
    resident_set = rss * page_size_kb;
}

void prf_analysis() {

  double scan_delta   = 0.1;
  int scan_Nsteps     = 1000;
  double scan_step    = 2. * scan_delta / scan_Nsteps;

  //bool PLOT = true;
  //bool DEBUG = true;

  vector<TString> listOfFiles;
  TString file    = "default.root";
  TString prefix  = "~/";
  Int_t iter = 0;
  TString add_name = "";
  Int_t add_name_i = 0;
  for (int iarg=0; iarg<gApplication->Argc(); iarg++){
    if (string( gApplication->Argv(iarg))=="-f" || string( gApplication->Argv(iarg))=="--file" ){
      iarg++;
      file = gApplication->Argv(iarg);
      if (file.Contains(".root")) {

        cout << "adding filename" <<" " << file << endl;
        listOfFiles.push_back(file);
      } else {
        fstream fList(file);
        if (fList.good()) {
          while (fList.good()) {
            string filename;
            getline(fList, filename);
            if (fList.eof()) break;
            listOfFiles.push_back(filename);
          }
        }
      }
    } else if (string( gApplication->Argv(iarg))=="-o" || string( gApplication->Argv(iarg))=="--output" ){
      iarg++;
      prefix = gApplication->Argv(iarg);
    } else if (string( gApplication->Argv(iarg))=="-i" || string( gApplication->Argv(iarg))=="--iter" ){
      iarg++;
      iter = stoi(gApplication->Argv(iarg));
    } else if (string( gApplication->Argv(iarg))=="-n" || string( gApplication->Argv(iarg))=="--number" ){
      iarg++;
      add_name = gApplication->Argv(iarg);
      add_name_i = stoi(gApplication->Argv(iarg));
    } else if (string( gApplication->Argv(iarg))=="-h" || string( gApplication->Argv(iarg))=="--help" ){
      cout << "**************************************" << endl;
      cout << "Macros run options:" << endl;
      cout << "   -f || --file      input file (*.root) or filelist" << endl;
      cout << "   -o || --output    output folder for plots and out file" << endl;
      cout << "   -h || --help      print help info" << endl;
      cout << "**************************************" << endl;
    }
  }

  //TCanvas *c5 = new TCanvas("c5","evadc5", 400, 400, 400,300);
  //TCanvas *c4 = new TCanvas("c4","evadc4", 0, 400, 400,300);
  TH2F* PadDisplayGeo=new TH2F("PadDisplayGeo","X vs Y of hits", 38,-18.13,18.13, 50,-17.15,17.15);
  TFile* out_file = new TFile((prefix + "/output_prf_drift_" + add_name + ".root").Data(), "RECREATE");
  out_file->cd();

  float prf_min   = -1.5;
  float prf_max   = 1.5;
  int prf_bin     = 120;

  float resol_min   = -0.3;
  float resol_max   = 0.3;
  int resol_bin     = 75.;

  const int drift_bins = 26;
  TAxis* drift_axis = new TAxis(drift_bins, 0., 520.);

double uncertainty[drift_bins] = {1.25407, 1.00371, 0.828022, 0.157767, 0.157039, 0.136241, 0.115883, 0.106854, 0.103353, 0.102694, 0.1073, 0.107885, 0.109452, 0.11389, 0.116545, 0.119209, 0.121429, 0.123965, 0.12642, 0.128895, 0.130169, 0.128467, 0.136043, 0.33439, 0.7, 0.7};
double co[drift_bins] = {0.0308268, 0.129235, 0.0864788, 0.752218, 0.821929, 0.855386, 0.867715, 0.890869, 0.878705, 0.878916, 0.869312, 0.880204, 0.872842, 0.864124, 0.832183, 0.800795, 0.803459, 0.796078, 0.807157, 0.799365, 0.794218, 0.781005, 0.762687, 0.56988, 0.00512019, 0.00512019};
double a2[drift_bins] = {1.17689e+08, 1.17251, 1774.04, -2.52486, -2.22959, -2.19962, -2.64604, -1.06105, -1.24373, -0.111186, -0.647679, -0.572204, -1.34067, -0.819704, 0.80085, 17.3457, 1.85339, 1.51431, -1.35838, -0.489907, 0.989999, -0.518657, 3.93012, 0.87238, 35.2735, 35.2735};
double a4[drift_bins] = {-4.66685e+07, -0.922864, -642.602, 2.60295, 3.10481, 2.95568, 3.11168, 1.10502, 1.28464, 0.576252, 0.585223, 0.417036, 0.777635, 0.319649, -0.216664, -10.2347, 0.581979, 0.235573, 0.836957, 0.592508, 0.146184, 0.637917, 0.711799, -0.435788, -22.7354, -22.7354};
double b2[drift_bins] = {6.13579e+08, 0.99009, 1044.4, -1.80911, -0.790269, -1.53545, -2.88145, -0.0556536, -0.798793, 0.500925, 0.325437, 1.00459, 0.284103, 0.88905, 1.83388, 14.7923, 1.67278, 1.79528, 0.148478, 0.691675, 1.94176, 0.512555, 3.39794, 7.1483, 36.8392, 36.8392};
double b4[drift_bins] = {-341707, -0.734177, -402.247, 6.32459, 12.8021, 12.4679, 13.5207, 8.61658, 9.19066, 12.1695, 7.87256, 5.96215, 3.11386, 3.69799, 9.32682, 49.1719, 18.147, 13.8875, 2.13926, 5.0883, 10.015, 4.89688, 23.5244, -2.40688, -23.7882, -23.7882};

  TF1* f1[drift_bins];

  /*double c = 0.8;
  double r = 0.5;
  double s = 0.7;*/

  for (int i = 0; i < drift_bins; ++i) {
    f1[i] = new TF1("f1", "[0]*(1+[1]*x*x + [2] * x*x*x*x) / (1+[3]*x*x+[4]*x*x*x*x)", -3, 3);
    f1[i]->SetParName(0, "Const");
    f1[i]->SetParName(1, "a2");
    f1[i]->SetParName(2, "a4");
    f1[i]->SetParName(3, "b2");
    f1[i]->SetParName(4, "b4");

    f1[i]->SetParameters(co[i], a2[i], a4[i], b2[i], b4[i]);
  }

  const int Niter = 1;
  TH2F* PRF_H[Niter][drift_bins];
  /*TH2F* PRF_H_2pad[Niter];
  TH2F* PRF_H_3pad[Niter];
  TH2F* PRF_H_4pad[Niter];*/
  TGraphErrors* PRF_gr[Niter][drift_bins];
  TGraph* PRF_sc[Niter][drift_bins];
  TH1F* Chi2_track[Niter][drift_bins];
  TH2F* Cluster_angle = new TH2F("cluster_angle", "", 520, 0., 520., 30, -0.75, 0.75);
  TH2F* hit_map_YZ    = new TH2F("hit_map_YZ", "", 520, 0., 520., 48, 0., 48.);

  for (int drift_id = 0; drift_id < drift_bins; ++drift_id)
    uncertainty[drift_id] = 0.;

  TH1F* resol_col_hist[Niter][48][drift_bins];
  TH1F* resol_col_hist_1[Niter][48][drift_bins];

  /*TH1F* resol_col[Niter][drift_bins];
  TH1F* resol_col1[Niter][drift_bins];
  TH1F* resol_colf[Niter][drift_bins];

  TH1F* mean_col[Niter][drift_bins];
  TH1F* mean_col1[Niter][drift_bins];
  TH1F* mean_colf[Niter][drift_bins];*/

   for (Int_t i = 0; i < Niter; ++i) {
    for (int drift_id = 0; drift_id < drift_bins; ++drift_id) {

      PRF_H[i][drift_id] = new TH2F(Form("PRF_%i_%i", i, drift_id),"", prf_bin, prf_min, prf_max, 102,0.,1.02);
      /*PRF_H_2pad[i][drift_id] = new TH2F(Form("PRF_2pad_%i", i),"", prf_bin, prf_min, prf_max, 102,0.,1.02);
      PRF_H_3pad[i][drift_id] = new TH2F(Form("PRF_3pad_%i", i),"", prf_bin, prf_min, prf_max, 102,0.,1.02);
      PRF_H_4pad[i][drift_id] = new TH2F(Form("PRF_4pad_%i", i),"", prf_bin, prf_min, prf_max, 102,0.,1.02);*/

      PRF_sc[i][drift_id] = new TGraph();
      PRF_sc[i][drift_id]->SetName(Form("prf_graph_%i_%i", i, drift_id));
      PRF_sc[i][drift_id]->SetMaximum(1.02);

      PRF_gr[i][drift_id] = new TGraphErrors();
      PRF_gr[i][drift_id]->SetName(Form("prf_graph_err_%i_%i", i, drift_id));
      PRF_gr[i][drift_id]->SetMaximum(1.02);

      Chi2_track[i][drift_id] = new TH1F(Form("Chi2_track_%i_%i", i, drift_id), "", 100, 0., 3.);

      for (Int_t j = 0; j < 48; ++j) {
        resol_col_hist[i][j][drift_id]  = new TH1F(Form("resol_histo_%i_%i_%i", i, j, drift_id), "", resol_bin, resol_min, resol_max);
        resol_col_hist_1[i][j][drift_id]  = new TH1F(Form("resol_histo1_%i_%i_%i", i, j, drift_id), "", resol_bin, resol_min, resol_max);
      }
      /*resol_col[i][drift_id]          = new TH1F(Form("resol_%i_%i", i, drift_id), "", 48, 0., 48.);
      resol_col1[i][drift_id]          = new TH1F(Form("resol1_%i_%i", i, drift_id), "", 48, 0., 48.);
      resol_colf[i][drift_id]          = new TH1F(Form("resol_final_%i_%i", i, drift_id), "", 48, 0., 48.);

      mean_col[i][drift_id]          = new TH1F(Form("mean_%i_%i", i, drift_id), "", 48, 0., 48.);
      mean_col1[i][drift_id]          = new TH1F(Form("mean1_%i_%i", i, drift_id), "", 48, 0., 48.);
      mean_colf[i][drift_id]          = new TH1F(Form("mean_final_%i_%i", i, drift_id), "", 48, 0., 48.);*/
    }
  }

  vector< int > Entries_sel;

  //************************************************************
  //****************** LOOP OVER FILES  ************************
  //************************************************************
  TChain* geo_chain = new TChain("femGeomTree");
  TChain* data      = new TChain("padData");

  for (uint ifile=0;ifile< listOfFiles.size();ifile++){
    file = listOfFiles[ifile];
    data->AddFile(file);
    cout << file << endl;
    if (ifile == 0)
      geo_chain->AddFile(file);
  }
  vector<int> *iPad(0);
  vector<int> *jPad(0);
  vector<double> *xPad(0);
  vector<double> *yPad(0);

  vector<double> *dxPad(0);
  vector<double> *dyPad(0);

  geo_chain->SetBranchAddress("jPad", &jPad );
  geo_chain->SetBranchAddress("iPad", &iPad );

  geo_chain->SetBranchAddress("xPad", &xPad );
  geo_chain->SetBranchAddress("yPad", &yPad );

  geo_chain->SetBranchAddress("dxPad", &dxPad );
  geo_chain->SetBranchAddress("dyPad", &dyPad );
  geo_chain->GetEntry(0); // put into memory geometry info
  cout << "***************************" << endl;
  cout << "Reading channel mapping" << endl;
  cout << "jPad->size()  " << jPad->size() <<endl;
  cout << "iPad->size()  " << iPad->size() <<endl;

  cout << "Reading geometry" << endl;
  cout << "xPad->size()  " << xPad->size() <<endl;
  cout << "yPad->size()  " << yPad->size() <<endl;
  cout << "dxPad->size() " << dxPad->size() <<endl;
  cout << "dyPad->size() " << dyPad->size() <<endl;

  int Imax = -1;
  int Imin = 10000000;
  int Jmax = -1;
  int Jmin = 10000000;
  double Xmax = -1;
  double Xmin = 10000000;
  double Ymax = -1;
  double Ymin = 10000000;


  for (unsigned long i = 0; i < jPad->size(); ++i) {
    if (Imax < (*iPad)[i])
      Imax = (*iPad)[i];
    if (Imin > (*iPad)[i])
      Imin = (*iPad)[i];
    if (Jmax < (*jPad)[i])
      Jmax = (*jPad)[i];
    if (Jmin > (*jPad)[i])
      Jmin = (*jPad)[i];
    if (Xmax < (*xPad)[i])
      Xmax = (*xPad)[i];
    if (Xmin > (*xPad)[i])
      Xmin = (*xPad)[i];
    if (Ymax < (*yPad)[i])
      Ymax = (*yPad)[i];
    if (Ymin > (*yPad)[i])
      Ymin = (*yPad)[i];
  }

  cout << "***************************" << endl;
  cout << "J goes from " << Jmin << "\t to " << Jmax << endl;
  cout << "I goes from " << Imin << "\t to " << Imax << endl;
  cout << "X goes from " << Xmin << "\t to " << Xmax << endl;
  cout << "Y goes from " << Ymin << "\t to " << Ymax << endl;
  cout << "X and Y size are " << (*dxPad)[0] << "\t" << (*dyPad)[0] << endl;

  pair<Float_t, Float_t> geom[48][36];
  for (UInt_t it = 0; it < (*iPad).size(); ++it) {
    geom[(*jPad)[it]][(*iPad)[it]].first    = (*xPad)[it];
    geom[(*jPad)[it]][(*iPad)[it]].second   = (*yPad)[it];
  }

  gStyle->SetPalette(1);
  gStyle->SetOptStat(0);
  int Nevents=0;

  vector<short>          *listOfChannels(0);
  vector<vector<short> > *listOfSamples(0);

  data->SetBranchAddress("PadphysChannels", &listOfChannels );
  data->SetBranchAddress("PadADCvsTime"   , &listOfSamples );

  cout <<data->GetEntries() << " evts in file " << endl;

  if (Nevents<=0) Nevents=data->GetEntries();
  if (Nevents>data->GetEntries()) Nevents=data->GetEntries();

  Int_t total_events  = 0;
  Int_t reco_tracks   = 0;
  Int_t sel_tracks    = 0;

  total_events = reco_tracks = sel_tracks = 0.;
  //if (iter == 0)
    for (Int_t i = 0; i < Nevents; ++i)
      Entries_sel.push_back(i);

  TString EventListFile = "/afs/cern.ch/work/s/ssuvorov/dev/TPC_TestBeam/scripts/list/cosm_";
  TString file_add[28] = {"386", "387", "388", "389", "390", "392", "393", "394", "395", "396", "397", "399", "400", "401", "402", "403", "404", "405", "406", "407", "408", "409", "410", "411", "412", "413", "414", "384"};

  EventListFile += file_add[add_name_i];
  EventListFile += ".list";

  /*std::ifstream fList(EventListFile.Data());
  if(fList.good()) {
    while(fList.good()) {
      std::string ev;
      getline(fList, ev);
      if(fList.eof()) break;

      Entries_sel.push_back(stoi(ev.c_str()));
    }
  }*/

  Nevents = Entries_sel.size();

  cout << "[                                                  ] Nevents = " << Nevents << "\r[";

  uint ievt = 0;
  uint evt_num = 0;
  uint ievt_dot = 0;
  uint step = Nevents/50;

  while (ievt != Entries_sel.size()) {
    if (evt_num > ievt_dot) {
      cout <<"."<<flush;
      ievt_dot += step;
      double vm, rss;
      process_mem_usage(vm, rss);
      //cout << "Evt  " << ievt << " Ent  " <<  Entries_sel[ievt]  << "      VM: " << vm << "; RSS: " << rss << endl;
    }
    ++evt_num;
    vector< vector<short> > PadDisplay;
    vector< vector<short> > PadTime;
    vector< vector<short> > PadDisplayNew;
    vector<vector< vector<short> > > MultiOutPad;
    vector<vector< vector<short> > > MultiOutPadTime;

    vector< vector< vector<short> > > PadDisplay3D;

    Nevents = Entries_sel.size();

    data->GetEntry(Entries_sel[ievt]);
    // clean for the next event
    PadDisplayNew.clear();
    //PadDisplay.clear();
    vector< vector<short> >().swap(PadDisplay);
    PadTime.clear();
    MultiOutPad.clear();
    MultiOutPadTime.clear();

    if (!(*listOfSamples).size()) {
      Entries_sel.erase(Entries_sel.begin() + ievt);
      continue;
    }

    int time_bins = (*listOfSamples)[0].size();

    //PadDisplay3D.clear();
    vector< vector< vector<short> > >().swap(PadDisplay3D);
    PadDisplay3D.resize(Jmax+1);
    for (int z=0; z <= Jmax; ++z) {
      PadDisplay3D[z].resize(Imax+1);
      for (int t = 0; t <= Imax; ++t)
        PadDisplay3D[z][t].resize(time_bins, 0);
    }

    PadDisplay.resize(Jmax+1);
    for (int z=0; z <= Jmax; ++z)
      PadDisplay[z].resize(Imax+1, 0);

    PadTime.resize(Jmax+1);
    for (int z=0; z <= Jmax; ++z)
      PadTime[z].resize(Imax+1, 0);

    PadDisplayNew.resize(Jmax+1);
    for (int z=0; z <= Jmax; ++z)
      PadDisplayNew[z].resize(Imax+1, 0);

    //************************************************************
    //*****************LOOP OVER CHANNELS ************************
    //************************************************************
    for (uint ic=0; ic< listOfChannels->size(); ic++){
      int chan= (*listOfChannels)[ic];
      // find out the maximum
      float adcmax=-1;
      Int_t it_t_max = -1;

      // one maximum per channel
      for (uint it = TIME_FIRST; it < TIME_LAST; it++){
      //for (uint it = 10; it < 450; it++){
        int adc= (*listOfSamples)[ic][it];
        PadDisplay3D[(*jPad)[chan]][(*iPad)[chan]][it] = adc;
        if (adc>adcmax && adc) {
          adcmax = adc;
          it_t_max = it;
        }
      }

      if (adcmax < 0) continue; // remove noise

      // noise suppression
      if (it_t_max < TIME_FIRST_REL || it_t_max > TIME_LAST_REL)
        continue;

      PadDisplay[(*jPad)[chan]][(*iPad)[chan]]  = adcmax;
      PadTime[(*jPad)[chan]][(*iPad)[chan]]     = it_t_max;
    } //loop over channels

    ++total_events;
#ifndef SEVERE_SUPPRESSION
    Int_t gErrorIgnoreLevel_bu = gErrorIgnoreLevel;
    if (SUPPRESS_ERRORS) {
      gErrorIgnoreLevel_bu = gErrorIgnoreLevel;
      gErrorIgnoreLevel = kSysError;
    }
#endif
    // apply the pattern recognition
    if (!TestSelection3D_cosmic(PadDisplay, PadTime, PadDisplayNew, MultiOutPad, MultiOutPadTime, PadDisplay3D)) {
    //if (!DBSCANSelectionCosmic(PadDisplay, PadTime, PadDisplayNew, MultiOutPad, MultiOutPadTime)) {
      Entries_sel.erase(Entries_sel.begin() + ievt);
      continue;
    }

#ifndef SEVERE_SUPPRESSION
    if (SUPPRESS_ERRORS)
      gErrorIgnoreLevel = gErrorIgnoreLevel_bu;
#endif

    // multiplicity - need one track
    /*if (MultiOutPad.size() > 1) {
      Entries_sel.erase(Entries_sel.begin() + ievt);
      continue;
    }*/

    bool useful_track_found = false;

    for (uint trackId = 0; trackId < MultiOutPad.size(); ++trackId) {
      ++reco_tracks;

      PadDisplayGeo->Reset();
      FillPadDisplayGeo(MultiOutPad[trackId], PadDisplayGeo, geom);

      // apply selection
      if (!CosmicLeft(MultiOutPad[trackId], 25.))
        continue;

      TVector3 start, end;
      GetTrackPos_3D(MultiOutPad[trackId], MultiOutPadTime[trackId], start, end, true);

      start.SetX(start.X() * 0.7);
      start.SetY(start.Y() * 0.9);
      start.SetZ((start.Z() - 57) * 0.08 * 5.3);

      end.SetX(end.X() * 0.7);
      end.SetY(end.Y() * 0.9);
      end.SetZ((end.Z() - 57) * 0.08 * 5.3);

      TVector3 track_v = end-start;
      track_v = track_v.Unit();
      Float_t thetaZ = TMath::ATan(track_v.Z() / track_v.X());
      //// if the angle cut is aplied in XY or in XYZ
      track_v.SetZ(0.);
      ////
      track_v = track_v.Unit();
      if (abs(track_v.X()) < 0.9)
        continue;
      //if (abs((end-start).Unit().X()) < 0.9)
      //  continue;

      //cout << (end-start).Unit().X() << "   " << (end-start).Unit().Y() << "    " << (end-start).Unit().Z() << endl;
      //cout << "cos theta " << (end-start).Unit().X() << endl;

      // interested in the certain drift region
      Float_t time = GetAverageTime(MultiOutPad[trackId], MultiOutPadTime[trackId]);
      int drift_bin = drift_axis->FindBin(time) - 1;

      // selection is done
      useful_track_found = true;
      ++sel_tracks;

      TGraphErrors* track = new TGraphErrors();
      TGraphErrors* track_m = new TGraphErrors();

      TGraphErrors* track_1[48];
      for (int i = 0; i <= Jmax; ++i) {
        track_1[i] = new TGraphErrors();
      }

      int cluster[48];
      int cluster_N[48];
      int cluster_time[48];
      double true_track[48];
      double cluster_mean[48];
      float charge_max[48];

      // At the moment ommit first and last column
      // first loop over column
      for (Int_t it_j = 1; it_j < Jmax; ++it_j) {
        cluster[it_j]     = 0;
        cluster_N[it_j]   = 0;
        cluster_time[it_j]= 0;
        charge_max[it_j]  = 0;
        true_track[it_j]  = -999.;
        cluster_mean[it_j] = 0.;

        TH1F* cluster_h = new TH1F("cluster", "", 38,-18.3,18.3);

        for (Int_t it_i = 0; it_i <= Imax; ++it_i) {
          if (!MultiOutPad[trackId][it_j][it_i])
            continue;

          //cout << "j:i " << it_j << ":" << it_i << endl;

          cluster[it_j] += MultiOutPad[trackId][it_j][it_i];
          ++cluster_N[it_j];
          cluster_h->Fill(geom[0][it_i].first, MultiOutPad[trackId][it_j][it_i]);
          if (charge_max[it_j] < MultiOutPad[trackId][it_j][it_i]) {
            charge_max[it_j] = MultiOutPad[trackId][it_j][it_i];
            cluster_time[it_j] = MultiOutPadTime[trackId][it_j][it_i];
          }
        }

        //cout << "It_j: " << it_j << "  \tcluster N " << cluster_N[it_j] << "\tmean " << cluster_h->GetMean() << "\tmax " << charge_max[it_j] << "\tat time " << cluster_time[it_j] << endl;

        cluster_mean[it_j] = cluster_h->GetMean();

        delete cluster_h;

        if (!cluster[it_j] || !charge_max[it_j])
          continue;

        // minimise chi2 to extimate true_track
        double chi2_min     = 1e9;
        double scan_y       = cluster_mean[it_j] - scan_delta;

        if (iter > 0) {
          for (Int_t scanId = 0; scanId < scan_Nsteps; ++scanId) {
            double chi2 = 0;
            for (Int_t it_i = 0; it_i <= Imax; ++it_i) {
              if (!MultiOutPad[trackId][it_j][it_i])
                continue;

              time = MultiOutPadTime[trackId][it_j][it_i];
              drift_bin = drift_axis->FindBin(time) - 1;

              if (drift_bin < 0 || drift_bin >= drift_bins)
                cerr << "ALARM!! OO drift bin 1 " << drift_bin << "  time " << time << endl;

              double a = 1. * MultiOutPad[trackId][it_j][it_i] / cluster[it_j];
              double center_pad_y = geom[0][it_i].first;
              double part = (a - f1[drift_bin]->Eval(scan_y - center_pad_y));
              double b = 1.*cluster[it_i];
              double c = 1.*MultiOutPad[trackId][it_j][it_i];
              part *= c*c;
              part /= c*sqrt(b) + b*sqrt(c);
              part *= part;

              chi2 += part;
            }

            if (chi2 < chi2_min) {
              chi2_min = chi2;
              true_track[it_j] = scan_y;
            }

            scan_y += scan_step;
          }
        }

        double x = geom[it_j][0].second;

        if (iter == 0)
          true_track[it_j] = cluster_mean[it_j];

        track->SetPoint(track->GetN(), x, true_track[it_j]);
        //cout << "x:y " << x << ":" <<  true_track[it_j] << endl;
        for (int i = 1; i < Jmax; ++i) {
          if (i != it_j) {
            track_1[i]->SetPoint(track_1[i]->GetN(), x, true_track[it_j]);
          }
        }

        track_m->SetPoint(track_m->GetN(), x, cluster_mean[it_j]);
        double error;

        drift_bin = drift_axis->FindBin(cluster_time[it_j]) - 1;
        if (drift_bin < 0 || drift_bin >= drift_bins)
          cerr << "ALARM!! OO drift bin 2 " << drift_bin << "  time " << cluster_time[it_j] << endl;

        Cluster_angle->Fill(cluster_time[it_j], thetaZ);
        hit_map_YZ->Fill(cluster_time[it_j], it_j);

        if (cluster_N[it_j] == 1)
          error = 0.3;
        else {
          if (iter == 0)
            error = 0.1;
          else
            error = uncertainty[drift_bin];
        }
        track->SetPointError(track->GetN()-1, 0., error);
        for (int i = 1; i < Jmax; ++i) {
          if (i != it_j) {
            track_1[i]->SetPointError(track_1[i]->GetN() - 1, 0., error);
          }
        }
      } // loop over j

      track->Fit("pol1", "Q");
      TF1* fit = track->GetFunction("pol1");

      if (!fit)
        continue;

      if (!fit->GetNDF())
        continue;

      double quality = fit->GetChisquare() / fit->GetNDF();
      double k = fit->GetParameter(1);
      double b = fit->GetParameter(0);

      //cout << "k = " << k << "  b = " << b << "  q = " << quality <<  endl;
      if (quality < 0.001)
        continue;

      Chi2_track[0][drift_bin]->Fill(quality);

      double k1[48];
      double b1[48];

      for (int i = 1; i < Jmax; ++i) {
        track_1[i]->Fit("pol1", "Q");
        TF1* fit1 = track_1[i]->GetFunction("pol1");

        if (!fit1)
          cout << "alarm " << track_1[i]->GetN() << "   " << track->GetN() << endl;

        k1[i] = fit1->GetParameter(1);
        b1[i] = fit1->GetParameter(0);
        delete fit1;
      }

      // second loop over columns
      for (Int_t it_j = 1; it_j < Jmax; ++it_j) {

        if (true_track[it_j]  == -999.)
          continue;

        double x    = geom[it_j][0].second;
        double track_fit_y  = k * x + b;
        double track_fit_y1 = k1[it_j] * x + b1[it_j];
        //double track_fit_y1 = k1 * x + b1;
        //cout << k << "\t" << b << "\t" << x << endl;

        drift_bin = drift_axis->FindBin(cluster_time[it_j]) - 1;
        if (drift_bin < 0 || drift_bin >= drift_bins)
          cerr << "ALARM!! OO drift bin 3 " << drift_bin << "  time " << cluster_time[it_j] << endl;

        // fill SR
        resol_col_hist[0][it_j][drift_bin]->Fill(true_track[it_j] - track_fit_y);
        resol_col_hist_1[0][it_j][drift_bin]->Fill(true_track[it_j] - track_fit_y1);

        //if (drift_bin > 10 && drift_bin < 18) {
          //cout << "Row \t" << it_j << " \tXtrue - Xreco \t" << true_track[it_j] - track_fit_y << "\tXtrue\t" << true_track[it_j] << "\tXreco\t" << track_fit_y  << "\tdrift_bin\t" << drift_bin << endl;
        //}

        //cout << "prf:COC:fit\t" << true_track[it_j] << " : "  << cluster_mean[it_j] << " : " << track_fit_y << endl;

        if (cluster_N[it_j] == 1)
          continue;
        // Fill PRF
        for (Int_t it_i = 0; it_i <= Imax; ++it_i) {
          if (!cluster[it_j] || !MultiOutPad[trackId][it_j][it_j])
            continue;

          double charge = 1. * MultiOutPad[trackId][it_j][it_i] / cluster[it_j];
          double center_pad_y = geom[0][it_i].first;

          time = MultiOutPadTime[trackId][it_j][it_i];
          drift_bin = drift_axis->FindBin(time) - 1;
          if (drift_bin < 0 || drift_bin >= drift_bins)
            cerr << "ALARM!! OO drift bin 4 " << drift_bin << "  time " << time << endl;

          // fill PRF
          PRF_H[0][drift_bin]->Fill(center_pad_y - track_fit_y, charge);
          PRF_sc[0][drift_bin]->SetPoint(PRF_sc[0][drift_bin]->GetN(), center_pad_y - track_fit_y, charge);

        }
      } // loop over colums

      /*c5->cd();
      c5->Clear();
      PadDisplayGeo->Draw("colz");
      c5->Modified();
      c5->Update();
      gPad->Modified();
      gPad->Update();
      //c5->WaitPrimitive();

      c4->cd();
      c4->Clear();
      track->Draw("ap");
      c4->Modified();
      c4->Update();
      gPad->Modified();
      gPad->Update();
      c4->WaitPrimitive();*/

      delete track;
      delete track_m;
      for (int i = 0; i <= Jmax; ++i)
        delete track_1[i];
    } // loop over tracks
    if (!useful_track_found) {
      Entries_sel.erase(Entries_sel.begin() + ievt);
      continue;
    }

    //cout << Entries_sel[evt_num] << endl;
    ++ievt;
  } // loop over events
  cout << "]" << endl;

  out_file->cd();

  cout << "Total events number     : " << total_events << endl;
  cout << "Reco tracks             : " << reco_tracks << endl;
  cout << "Selected tracks         : " << sel_tracks << endl;

  cout << "iter " << iter << endl;

  for (Int_t drift_id = 0; drift_id < drift_bins; ++drift_id) {
    PRF_sc[0][drift_id]->Write();
    PRF_H[0][drift_id]->Write();

    for (Int_t i = 1; i < 47; ++i) {
      resol_col_hist[0][i][drift_id]->Write();
      resol_col_hist_1[0][i][drift_id]->Write();
    }
  } // loop over drift id

  drift_axis->SetName("drift_axis");
  drift_axis->Write();
  Cluster_angle->Write();
  hit_map_YZ->Write();

  cout << "Done" << endl;
  out_file->Close();
  exit(1);
}

