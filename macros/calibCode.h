//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Mon Dec 17 14:54:17 2018 by ROOT version 6.14/04
// from TTree treeInfo/tracks info
// found on file: run00381.root.root
//////////////////////////////////////////////////////////

#ifndef calibCode_h
#define calibCode_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.
#include "vector"
#include "vector"

class calibCode {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   Int_t           ev;
   vector<int>     *x;
   vector<int>     *y;
   vector<float>   *charge;

   // List of branches
   TBranch        *b_ev;   //!
   TBranch        *b_x;   //!
   TBranch        *b_y;   //!
   TBranch        *b_charge;   //!

   calibCode(TTree *tree=0);
   virtual ~calibCode();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t 
   e(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef calibCode_cxx
calibCode::calibCode(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("cosmic4.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("cosmic4.root");
      }
      f->GetObject("treeInfo",tree);

   }
   Init(tree);
}

calibCode::~calibCode()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t calibCode::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t calibCode::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void calibCode::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // calibCode, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set object pointer
   x = 0;
   y = 0;
   charge = 0;
   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("ev", &ev, &b_ev);
   fChain->SetBranchAddress("x", &x, &b_x);
   fChain->SetBranchAddress("y", &y, &b_y);
   fChain->SetBranchAddress("charge", &charge, &b_charge);
   Notify();
}

Bool_t calibCode::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated calibCode, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void calibCode::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t calibCode::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef calibCode_cxx
