# !bin/sh

i=0
while IFS=$'\n' read file || [[ -n "$file" ]]; do
  echo "#!/bin/bash" > "${i}.sh"
  echo "" >> "${i}.sh"
  echo "echo \"job ${i}\"" >> "${i}.sh"
  echo "source /cvmfs/sft.cern.ch/lcg/contrib/gcc/4.8/x86_64-centos7-gcc48-opt/setup.sh" >> "${i}.sh"
  echo "source /cvmfs/sft.cern.ch/lcg/app/releases/ROOT/6.14.04/x86_64-centos7-gcc48-opt/root/bin/thisroot.sh" >> "${i}.sh"
  echo "cd /afs/cern.ch/work/s/ssuvorov/dev/TPC_TestBeam/" >> "${i}.sh"
  echo "${file}" >> "${i}.sh"
  echo "${i}   ${file}"
  i=$((($i+1)))
done < "$1"